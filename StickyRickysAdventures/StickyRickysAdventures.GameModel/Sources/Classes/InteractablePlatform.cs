﻿// <copyright file="InteractablePlatform.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Classes
{
    using StickyRickysAdventures.GameModel.Sources.Interfaces;

    /// <summary>
    /// A platform which implements the IInteractable interface.
    /// </summary>
    public class InteractablePlatform : Platform
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InteractablePlatform"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the position rectangle.</param>
        /// <param name="y">The y coordinate of the position rectangle.</param>
        /// <param name="width">The width of the position rectangle.</param>
        /// <param name="height">The height of the position rectangle.</param>
        public InteractablePlatform(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
        }
    }
}
