﻿// <copyright file="HighScoreBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Classes
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameModel.Sources.Structs;
    using StickyRickysAdventures.GameRenderer.Interfaces;

    /// <summary>
    /// A concrete implementation of the <see cref="IListWithExitBuilder"/> interface. Exit is a button, list elements are scores.
    /// </summary>
    public class HighScoreBuilder : IListWithExitBuilder
    {
        private const int SCOREOFFSET = 3;
        private Grid menu;
        private IGameModel model;
        private IList<Score> scores;
        private int scoreLimit;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreBuilder"/> class.
        /// </summary>
        /// <param name="model">The <see cref="IGameModel"/> containing the window data.</param>
        /// <param name="scoreLimit">The number of scores we want to display.</param>
        /// <param name="scores">The list containing the scores.</param>
        public HighScoreBuilder(IGameModel model, int scoreLimit, IList<Score> scores)
        {
            this.model = model;
            this.scoreLimit = scoreLimit;
            this.scores = scores;
            this.InitMenu();
        }

        /// <inheritdoc/>
        public IListWithExitBuilder AddExit()
        {
            Button exit = new Button();
            exit.BorderBrush = null;
            exit.Content = "Exit to Main Menu";
            exit.Foreground = Brushes.White;
            exit.FontStyle = FontStyles.Italic;
            exit.Background = Brushes.DarkGreen;
            exit.MaxHeight = this.menu.Height / (this.scoreLimit + (2 * SCOREOFFSET));
            exit.MaxWidth = this.menu.Width / 3 * 0.4;
            exit.BorderBrush = Brushes.Brown;
            exit.BorderThickness = new Thickness(exit.MaxHeight * 0.05);

            this.menu.Children.Add(exit);
            Grid.SetColumn(exit, 1);
            Grid.SetRow(exit, this.scoreLimit + SCOREOFFSET + 1);
            return this;
        }

        /// <inheritdoc/>
        public IListWithExitBuilder AddListElements()
        {
            int i = 0;
            while (i < this.scores.Count && i < this.scoreLimit)
            {
                Label score = new Label();
                score.Content = (i + 1).ToString(CultureInfo.InvariantCulture) + ".\t" + this.scores[i].ToString();
                score.Foreground = Brushes.White;
                score.Background = Brushes.Transparent;
                score.FontStyle = FontStyles.Italic;
                score.FontSize = 15;
                score.HorizontalAlignment = HorizontalAlignment.Center;
                score.VerticalAlignment = VerticalAlignment.Center;
                score.Tag = this.scores[i];

                this.menu.Children.Add(score);
                Grid.SetColumn(score, 1);
                Grid.SetRow(score, SCOREOFFSET + i);
                i++;
            }

            return this;
        }

        /// <inheritdoc/>
        public IListWithExitBuilder AddTitle()
        {
            Label title = new Label();
            title.Content = "High Scores";
            title.Foreground = Brushes.AliceBlue;
            title.Background = Brushes.Transparent;
            title.FontFamily = new FontFamily("Papyrus");
            title.FontSize = 20;
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.VerticalAlignment = VerticalAlignment.Center;

            this.menu.Children.Add(title);
            Grid.SetColumn(title, 1);
            Grid.SetRow(title, 1);

            return this;
        }

        /// <inheritdoc/>
        public Grid GetHighScoreScreen()
        {
            return this.menu;
        }

        private void InitMenu()
        {
            this.menu = new Grid();
            for (int i = 0; i < this.scoreLimit + (SCOREOFFSET * 2); i++)
            {
                this.menu.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < 3; i++)
            {
                this.menu.ColumnDefinitions.Add(new ColumnDefinition());
            }

            this.menu.Width = this.model.WindowWidth;
            this.menu.Height = this.model.WindowHeight;

            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("StickyRickysAdventures.GameRenderer.Resources.MenuResources.Main_Menu_Background.png");
            if (bmp.StreamSource != null)
            {
                ImageBrush ib = new ImageBrush(bmp);
                this.menu.Background = ib;
                bmp.EndInit();
            }
        }
    }
}
