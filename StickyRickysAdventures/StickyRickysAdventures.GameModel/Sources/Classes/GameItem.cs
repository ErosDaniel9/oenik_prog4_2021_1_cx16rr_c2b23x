﻿// <copyright file="GameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources
{
    using System.Numerics;
    using System.Windows;
    using StickyRickysAdventures.GameModel.Sources.Interfaces;

    /// <summary>
    /// The abstract base class every object in the game inherits.
    /// </summary>
    public class GameItem : IGameItem
    {
        private Rect position;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameItem"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the position rectangle.</param>
        /// <param name="y">The y coordinate of the position rectangle.</param>
        /// <param name="width">The width of the position rectangle.</param>
        /// <param name="height">The height of the position rectangle.</param>
        public GameItem(int x, int y, int width, int height)
        {
            this.Position = new Rect(x, y, width, height);
        }

        /// <summary>
        /// Gets or sets the position of the element.
        /// </summary>
        public Rect Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.position = value;
            }
        }

        /// <summary>
        /// Gets the x coordinate of the position rectangle.
        /// </summary>
        /// <returns>The x coordinate of the position rectangle.</returns>
        public double GetX()
        {
            return this.Position.X;
        }

        /// <summary>
        /// Gets the y coordinate of the position rectangle.
        /// </summary>
        /// <returns>The y coordinate of the position rectangle.</returns>
        public double GetY()
        {
            return this.Position.Y;
        }

        /// <summary>
        /// Sets the x coordinate of the position rectangle.
        /// </summary>
        /// <param name="x">The new x component of the position rectangle.</param>
        public void SetX(double x)
        {
            this.position.X = x;
        }

        /// <summary>
        /// Sets the y coordinate of the position rectangle.
        /// </summary>
        /// <param name="y">The new y component of the position rectangle.</param>
        public void SetY(double y)
        {
            this.position.Y = y;
        }
    }
}
