﻿// <copyright file="Service.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Enums
{
    /// <summary>
    /// Consumables that get consumed instantly after buying them.
    /// </summary>
    public enum Service
    {
        /// <summary>
        /// Refills the bullets of the consumer.
        /// </summary>
        RefillBullets,
    }
}
