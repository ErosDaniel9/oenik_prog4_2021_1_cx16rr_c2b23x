﻿// <copyright file="LoadGameController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Controller
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using StickyRickysAdventures.GameLogic;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer;
    using StickyRickysAdventures.GameRenderer.Classes;
    using StickyRickysAdventures.Repository;

    /// <summary>
    /// Controller repsonsible for displaying the available saves, and loading the selected save.
    /// </summary>
    public class LoadGameController : FrameworkElement
    {
        private IGameModel gameModel;
        private MainRenderer renderer;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadGameController"/> class.
        /// </summary>
        /// <param name="gameModel">The game model containing the window data.</param>
        public LoadGameController(IGameModel gameModel)
        {
            this.gameModel = gameModel;
            this.Loaded += this.InitController;
        }

        private void InitController(object sender, RoutedEventArgs e)
        {
            MainWindow win = Application.Current.MainWindow as MainWindow;
            if (win != null)
            {
                IList<ISavePreviewModel> test = LoadMapLogic.GetEverySavedGame(new GameStateRepository());
                this.renderer = new MainRenderer(null, new LoadMenuBuilder(test, this.gameModel, new string[] { "Exit to Main Menu" }), null, null, null);
                Grid menu = this.renderer.GetMenu();
                win.Display.Children.Add(menu);
                win.RegisterName("Menu", menu);
            }
        }
    }
}
