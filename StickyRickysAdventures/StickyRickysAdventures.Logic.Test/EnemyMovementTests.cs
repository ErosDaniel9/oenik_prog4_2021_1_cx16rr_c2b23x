﻿// <copyright file="EnemyMovementTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Logic.Test
{
    using NUnit.Framework;
    using StickyRickysAdventures.GameModel.Sources.Classes;
    using StickyRickysAdventures.GameModel.Sources.Enums;

    /// <summary>
    /// Test class for enemy movements.
    /// </summary>
    public class EnemyMovementTests
    {
        /// <summary>
        /// Test for enemy movement.
        /// </summary>
        /// <param name="enemytilex">Enemy X tile.</param>
        /// <param name="enemytiley">Enemy Y tile.</param>
        /// <param name="d">Direction.</param>
        /// <param name="dx">DX.</param>
        [TestCase(3, 3, Direction.Left, -1)]
        [TestCase(3, 3, Direction.Right, 1)]
        public void TestEnemyMoves(int enemytilex, int enemytiley, Direction d, int dx)
        {
            GameModel.Models.MainGameModel model = Setups.GetModel() as GameModel.Models.MainGameModel;
            GameLogic.MainLogic logic = new GameLogic.MainLogic(model);
            CombatGameItem enemy = new MeleeEnemy(enemytilex * model.TileWidth, enemytiley * model.TileHeight, model.TileWidth, model.TileHeight);
            enemy.Direction = d;
            enemy.DX = dx;
            model.Enemies.Add(enemy);

            double x = enemy.GetX();

            logic.OneTick();
            Assert.That(x, Is.Not.EqualTo(enemy.GetX()));
        }
    }
}
