﻿// <copyright file="LoadMenuBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Classes
{
    using System.Collections.Generic;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer.Interfaces;

    /// <summary>
    /// The class responsible for building the elements for the load saves menu.
    /// </summary>
    public class LoadMenuBuilder : IMenuBuilder
    {
        private IList<ISavePreviewModel> saveModels;
        private IGameModel gameModel;
        private string[] extraButtonNames;
        private Grid menu;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadMenuBuilder"/> class.
        /// </summary>
        /// <param name="saveModels">The collection containing the necessary save informations to render.</param>
        /// <param name="gameModel">The game model containing the window parameters for our grid.</param>
        /// <param name="extraButtonNames">The name of the extra buttons. Optional parameter.</param>
        public LoadMenuBuilder(IList<ISavePreviewModel> saveModels, IGameModel gameModel, string[] extraButtonNames)
        {
            this.saveModels = saveModels;
            this.gameModel = gameModel;
            this.extraButtonNames = extraButtonNames;
            this.InitMenu();
        }

        /// <inheritdoc/>
        public IMenuBuilder AddButtons()
        {
            this.AddSaveButtons();
            this.AddExtraButtons();

            return this;
        }

        /// <inheritdoc/>
        public IMenuBuilder AddTitle()
        {
            Label title = new Label();
            title.Content = "Load Game";
            title.Foreground = Brushes.AliceBlue;
            title.Background = Brushes.Transparent;
            title.FontFamily = new FontFamily("Papyrus");
            title.FontSize = 20;
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.VerticalAlignment = VerticalAlignment.Center;

            this.menu.Children.Add(title);
            Grid.SetColumn(title, 1);
            Grid.SetRow(title, 1);
            return this;
        }

        /// <inheritdoc/>
        public Grid GetMenu()
        {
            return this.menu;
        }

        /// <inheritdoc/>
        public IMenuBuilder InitMenu()
        {
            this.menu = new Grid();
            for (int i = 0; i < this.saveModels.Count + this.extraButtonNames.Length + 4; i++)
            {
                this.menu.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < 3; i++)
            {
                this.menu.ColumnDefinitions.Add(new ColumnDefinition());
            }

            this.menu.Height = this.gameModel.WindowHeight;
            this.menu.Width = this.gameModel.WindowWidth;

            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("StickyRickysAdventures.GameRenderer.Resources.MenuResources.Main_Menu_Background.png");
            if (bmp.StreamSource != null)
            {
                ImageBrush ib = new ImageBrush(bmp);
                this.menu.Background = ib;
                bmp.EndInit();
            }

            return this;
        }

        private void AddExtraButtons()
        {
            for (int i = 0; i < this.extraButtonNames.Length; i++)
            {
                Button menuElement = new Button();

                menuElement.BorderBrush = null;
                menuElement.Content = this.extraButtonNames[i];
                menuElement.Foreground = Brushes.White;
                menuElement.FontStyle = FontStyles.Italic;
                menuElement.Background = Brushes.DarkGreen;
                menuElement.MaxHeight = this.menu.Height / this.menu.RowDefinitions.Count * 0.8;
                menuElement.MaxWidth = this.menu.Width * 0.7 / 3;
                menuElement.BorderBrush = Brushes.Brown;
                menuElement.BorderThickness = new Thickness(menuElement.MaxHeight * 0.05);

                this.menu.Children.Add(menuElement);
                Grid.SetColumn(menuElement, 1);
                Grid.SetRow(menuElement, i + this.saveModels.Count + 3);
            }
        }

        private void AddSaveButtons()
        {
            for (int i = 0; i < this.saveModels.Count; i++)
            {
                Button menuElement = new Button();

                menuElement.BorderBrush = null;
                menuElement.Content = this.saveModels[i].ToString(null, null);
                menuElement.Foreground = Brushes.White;
                menuElement.FontStyle = FontStyles.Italic;
                menuElement.Background = Brushes.DarkGreen;
                menuElement.MaxHeight = this.menu.Height / this.menu.RowDefinitions.Count * 0.8;
                menuElement.MaxWidth = this.menu.Width * 0.7 / 3;
                menuElement.BorderBrush = Brushes.Brown;
                menuElement.BorderThickness = new Thickness(menuElement.MaxHeight * 0.05);
                menuElement.Tag = this.saveModels[i];

                this.menu.Children.Add(menuElement);
                Grid.SetColumn(menuElement, 1);
                Grid.SetRow(menuElement, i + 3);
            }
        }
    }
}
