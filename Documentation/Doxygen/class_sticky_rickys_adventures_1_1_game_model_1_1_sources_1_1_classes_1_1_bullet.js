var class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet =
[
    [ "Bullet", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet.html#a7e70038f726f5c9a7b42e3d7cc2c22eb", null ],
    [ "Direction", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet.html#a566979c4c55911ddac310b89a0a75bcf", null ],
    [ "DX", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet.html#ad687f6634ba0135db37fdfe6f6b0c929", null ],
    [ "DY", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet.html#a24afa75395886731901f693ff5889a1c", null ],
    [ "MovementSpeed", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet.html#a56777e97c15cd005ec6b840f04b086fd", null ],
    [ "Owner", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet.html#a3dabd6732c9c615aeba4e5d256cacfa7", null ]
];