﻿// <copyright file="MetaController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;

[assembly:CLSCompliant(false)]

namespace StickyRickysAdventures.Controller
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using StickyRickysAdventures.GameLogic;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.Repository;

    /// <summary>
    /// The controller responsible for deciding which controller will be displaying data on the window.
    /// </summary>
    public class MetaController : FrameworkElement
    {
        private FrameworkElement currentController;
        private IGameModel gameModel;
        private Dictionary<string, Action> redirectMethods;
        private MainWindow win;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetaController"/> class.
        /// </summary>
        public MetaController()
        {
            this.Loaded += this.InitControl;
        }

        private void InitControl(object sender, RoutedEventArgs e)
        {
            this.win = Application.Current.MainWindow as MainWindow;
            this.win.WindowState = WindowState.Maximized;
            this.InitControllerRedirects();
            this.currentController = new MainMenuController();
            this.currentController.Loaded += this.SetControllerRedirects;
            this.UpdateGrid();
        }

        private void InitControllerRedirects()
        {
            this.redirectMethods = new Dictionary<string, Action>();
            this.redirectMethods.Add("MainMenuController", () => this.InitMainMenuRedirects());
            this.redirectMethods.Add("GameController", () => this.InitGameplayRedirects());
            this.redirectMethods.Add("InGameMenuController", () => this.InitInGameMenuRedirects());
            this.redirectMethods.Add("LoadGameController", () => this.InitLoadGameRedirects());
            this.redirectMethods.Add("ShopController", () => this.InitShopRedirects());
            this.redirectMethods.Add("HighScoreController", () => this.InitHighScoreRedirects());
            this.redirectMethods.Add("InstructionsController", () => this.InitInstructionRedirects());
        }

        private void InitInstructionRedirects()
        {
            Grid highScores = this.win.FindName("Instructions") as Grid;
            if (highScores != null)
            {
                foreach (var child in highScores.Children)
                {
                    if (child is Button && (child as Button).Content.Equals("Exit to Main Menu"))
                    {
                        (child as Button).Click += this.ExitToMainMenu;
                    }
                }
            }
        }

        private void SetControllerRedirects(object sender, RoutedEventArgs e)
        {
            string controllerType = this.currentController.GetType().Name;
            this.redirectMethods[controllerType]?.Invoke();
        }

        private void InitHighScoreRedirects()
        {
            Grid highScores = this.win.FindName("HighScores") as Grid;
            if (highScores != null)
            {
                foreach (var child in highScores.Children)
                {
                    if (child is Button && (child as Button).Content.Equals("Exit to Main Menu"))
                    {
                        (child as Button).Click += this.ExitToMainMenu;
                    }
                }
            }
        }

        private void InitMainMenuRedirects()
        {
            Grid menu = this.win.FindName("Menu") as Grid;
            if (menu != null)
            {
                foreach (var child in menu.Children)
                {
                    if (child is Button)
                    {
                        Button menuButton = child as Button;
                        switch (menuButton.Content)
                        {
                            case "New Game":
                                menuButton.Click += this.NewGame;
                                break;
                            case "Load Game":
                                menuButton.Click += this.LoadGame;
                                break;
                            case "Exit Game":
                                menuButton.Click += this.ExitGame;
                                break;
                            case "High Scores":
                                menuButton.Click += this.HighScores;
                                break;
                            case "Instructions":
                                menuButton.Click += this.Instructions;
                                break;
                        }
                    }
                }
            }
        }

        private void Instructions(object sender, RoutedEventArgs e)
        {
            this.gameModel = new MainGameModel((int)this.ActualWidth, (int)this.ActualHeight);
            this.currentController = new InstructionsController(this.gameModel);
            this.UnsubscribeFromAllEvents();
            this.currentController.Loaded += this.SetControllerRedirects;

            this.UpdateGrid();
        }

        private void HighScores(object sender, RoutedEventArgs e)
        {
            this.gameModel = new MainGameModel((int)this.ActualWidth, (int)this.ActualHeight);
            this.currentController = new HighScoreController(this.gameModel);
            this.UnsubscribeFromAllEvents();
            this.currentController.Loaded += this.SetControllerRedirects;

            this.UpdateGrid();
        }

        private void ExitGame(object sender, RoutedEventArgs e) => this.win?.Close();

        private void LoadGame(object sender, RoutedEventArgs e)
        {
            this.gameModel = new MainGameModel((int)this.ActualWidth, (int)this.ActualHeight);
            this.currentController = new LoadGameController(this.gameModel);
            this.UnsubscribeFromAllEvents();
            this.currentController.Loaded += this.SetControllerRedirects;

            this.UpdateGrid();
        }

        private void NewGame(object sender, RoutedEventArgs e)
        {
            this.gameModel = new MainGameModel((int)this.ActualWidth, (int)this.ActualHeight);
            LoadMapLogic.LoadMapFromTxt(this.gameModel, "StickyRickysAdventures.GameLogic.Maps.00.txt");

            this.currentController = new GameController(this.gameModel);
            this.UnsubscribeFromAllEvents();
            this.currentController.Loaded += this.SetControllerRedirects;

            this.UpdateGrid();
        }

        private void InitGameplayRedirects()
        {
            this.win.KeyDown += this.OpenAndCloseInGameMenu;
            this.win.KeyDown += this.OpenShop;
            Grid deathScreen = this.win.FindName("DeathScreen") as Grid;
            foreach (var child in deathScreen.Children)
            {
                if (child is Button && (child as Button).Content.Equals("Confirm"))
                {
                    (child as Button).Click += this.ExitToMainMenu;
                }
            }
        }

        private void OpenShop(object sender, KeyEventArgs e)
        {
            if (this.gameModel.Player.CurrentHealth >= 0 && e.Key == Key.E && this.gameModel.Player.Position.IntersectsWith(this.gameModel.Vendor))
            {
                this.UnsubscribeFromAllEvents();
                this.currentController = new ShopController(this.gameModel);
                this.currentController.Loaded += this.SetControllerRedirects;
                this.UpdateGrid();
            }
        }

        private void OpenAndCloseInGameMenu(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.UnsubscribeFromAllEvents();
                this.currentController = this.currentController is GameController ? new InGameMenuController(this.gameModel) : new GameController(this.gameModel);
                this.currentController.Loaded += this.SetControllerRedirects;

                this.UpdateGrid();
            }
        }

        private void InitInGameMenuRedirects()
        {
            this.win.KeyDown += this.OpenAndCloseInGameMenu;
            Grid menu = this.FindName("Menu") as Grid;
            if (menu != null)
            {
                foreach (var child in menu.Children)
                {
                    if (child is Button)
                    {
                        Button menuButton = child as Button;
                        switch (menuButton.Content)
                        {
                            case "Resume":
                                menuButton.Click += this.Resume;
                                break;
                            case "Exit to Main Menu":
                                menuButton.Click += this.ExitToMainMenu;
                                break;
                        } // TODO: generalize with reflection
                    }
                }
            }
        }

        private void Resume(object sender, RoutedEventArgs e)
        {
            this.UnsubscribeFromAllEvents();
            this.currentController = new GameController(this.gameModel);
            this.currentController.Loaded += this.SetControllerRedirects;
            this.UpdateGrid();
        }

        private void ExitToMainMenu(object sender, RoutedEventArgs e)
        {
            this.UnsubscribeFromAllEvents();
            this.currentController = new MainMenuController();
            this.currentController.Loaded += this.SetControllerRedirects;
            this.UpdateGrid();
        }

        private void InitShopRedirects()
        {
            this.win.KeyDown += this.ExitShopFromKeyboard;
            Grid shop = this.win.FindName("Shop") as Grid;
            if (shop != null)
            {
                foreach (var child in shop.Children)
                {
                    if (child is Button && (child as Button).Content.Equals("Exit"))
                    {
                        (child as Button).Click += this.ExitShopFromButton;
                    }
                }
            }
        }

        private void ExitShopFromButton(object sender, RoutedEventArgs e)
        {
            this.UnsubscribeFromAllEvents();
            this.currentController = new GameController(this.gameModel);
            this.currentController.Loaded += this.SetControllerRedirects;
            this.UpdateGrid();
        }

        private void ExitShopFromKeyboard(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.E)
            {
                this.UnsubscribeFromAllEvents();
                this.currentController = new GameController(this.gameModel);
                this.currentController.Loaded += this.SetControllerRedirects;
                this.UpdateGrid();
            }
        }

        private void InitLoadGameRedirects()
        {
            Grid menu = this.win.FindName("Menu") as Grid;
            if (menu != null)
            {
                foreach (var child in menu.Children)
                {
                    if (child is Button)
                    {
                        if ((child as Button).Content.Equals("Exit to Main Menu"))
                        {
                            (child as Button).Click += this.ExitToMainMenu;
                        }
                        else
                        {
                            (child as Button).Click += this.LoadFromSave;
                        }
                    }
                }
            }
        }

        private void LoadFromSave(object sender, RoutedEventArgs e)
        {
            int saveId = ((sender as Button).Tag as ISavePreviewModel).SaveID;
            this.gameModel = new MainGameModel((int)this.ActualWidth, (int)this.ActualHeight);
            var repo = new GameStateRepository();
            LoadMapLogic.LoadMapFromSave(saveId, repo, this.gameModel, this.gameModel.WindowWidth, this.gameModel.WindowHeight);

            this.UnsubscribeFromAllEvents();
            this.currentController = new GameController(this.gameModel);
            this.currentController.Loaded += this.SetControllerRedirects;
            this.UpdateGrid();
        }

        private void UpdateGrid()
        {
            if (this.win != null)
            {
                this.win.Display.Children.Clear();

                if (this.win.FindName("Menu") != null)
                {
                    this.win.UnregisterName("Menu");
                }

                if (this.win.FindName("Shop") != null)
                {
                    this.win.UnregisterName("Shop");
                }

                if (this.win.FindName("HighScores") != null)
                {
                    this.win.UnregisterName("HighScores");
                }

                if (this.win.FindName("DeathScreen") != null)
                {
                    this.win.UnregisterName("DeathScreen");
                }

                if (this.win.FindName("Instructions") != null)
                {
                    this.win.UnregisterName("Instructions");
                }

                this.win.Display.Children.Add(this.currentController);
            }
        }

        private void UnsubscribeFromAllEvents()
        {
            this.currentController.Loaded -= this.SetControllerRedirects;
            this.win.KeyDown -= this.ExitShopFromKeyboard;
            this.win.KeyDown -= this.OpenAndCloseInGameMenu;
            this.win.KeyDown -= this.OpenShop;

            this.UnsubMainMenuRedirects();
            this.UnsubInGameMenuRedirects();
        }

        private void UnsubInGameMenuRedirects()
        {
            Grid menu = this.win.FindName("Menu") as Grid;
            if (menu != null)
            {
                foreach (var child in menu.Children)
                {
                    if (child is Button)
                    {
                        Button menuButton = child as Button;
                        switch (menuButton.Content)
                        {
                            case "Resume":
                                menuButton.Click -= this.Resume;
                                break;
                            case "Exit to Main Menu":
                                menuButton.Click -= this.ExitToMainMenu;
                                break;
                        }
                    }
                }
            }
        }

        private void UnsubMainMenuRedirects()
        {
            Grid menu = this.win.FindName("Menu") as Grid;
            if (menu != null)
            {
                foreach (var child in menu.Children)
                {
                    if (child is Button)
                    {
                        Button menuButton = child as Button;
                        switch (menuButton.Content)
                        {
                            case "New Game":
                                menuButton.Click -= this.NewGame;
                                break;
                            case "Load Game":
                                menuButton.Click -= this.LoadGame;
                                break;
                            case "Exit Game":
                                menuButton.Click -= this.ExitGame;
                                break;
                        }
                    }
                }
            }
        }
    }
}
