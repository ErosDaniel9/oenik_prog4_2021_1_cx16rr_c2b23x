﻿// <copyright file="IInputGridBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Interfaces
{
    using System.Windows.Controls;

    /// <summary>
    /// The methods required to building an input grid.
    /// </summary>
    public interface IInputGridBuilder
    {
        /// <summary>
        /// Adds a message to the grid.
        /// </summary>
        /// <returns>The builder.</returns>
        public IInputGridBuilder AddMessage();

        /// <summary>
        /// Adds an input field to the grid.
        /// </summary>
        /// <returns>The builder.</returns>
        public IInputGridBuilder AddInput();

        /// <summary>
        /// Adds a confirmation button to the grid.
        /// </summary>
        /// <returns>The builder.</returns>
        public IInputGridBuilder AddConfirmationButton();

        /// <summary>
        ///  Returns the grid representing the input field.
        /// </summary>
        /// <returns>The grid representing the input field.</returns>
        public Grid GetInputGrid();
    }
}
