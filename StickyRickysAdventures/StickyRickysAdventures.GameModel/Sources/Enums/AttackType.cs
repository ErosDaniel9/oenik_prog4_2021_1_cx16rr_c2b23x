﻿// <copyright file="AttackType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Enums
{
    /// <summary>
    /// An enum representing the three different attack types our entities can use.
    /// </summary>
    public enum AttackType
    {
        /// <summary>
        /// Melee attack.
        /// </summary>
        Melee,

        /// <summary>
        /// Ranged attack.
        /// </summary>
        Ranged,

        /// <summary>
        /// Summon attack.
        /// </summary>
        Summon,
    }
}
