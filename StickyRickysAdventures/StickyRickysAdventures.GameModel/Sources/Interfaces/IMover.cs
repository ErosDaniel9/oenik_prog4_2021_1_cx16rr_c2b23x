﻿// <copyright file="IMover.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Interfaces
{
    using StickyRickysAdventures.GameModel.Sources.Enums;

    /// <summary>
    /// Methods and properties for all objects who can move in the game.
    /// </summary>
    public interface IMover
    {
        /// <summary>
        /// Gets or sets the x component of the speed vector of the mover.
        /// </summary>
        float DX { get; set; }

        /// <summary>
        /// Gets or sets the y component of the speed vector of the mover.
        /// </summary>
        float DY { get; set; }

        /// <summary>
        /// Gets or sets the direction of the mover.
        /// </summary>
        Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets the movement speed of the mover.
        /// </summary>
        double MovementSpeed { get; set; }
    }
}
