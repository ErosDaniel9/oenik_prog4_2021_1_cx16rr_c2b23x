﻿// <copyright file="IHighScoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Repository
{
    using System.Xml.Linq;

    /// <summary>
    /// The methods a high score repository must implement.
    /// </summary>
    public interface IHighScoreRepository
    {
        /// <summary>
        /// Returns all high scores from the XML file the repo points to.
        /// </summary>
        /// <returns>An XDocument containing all the high scores.</returns>
        XDocument GetHighScores();

        /// <summary>
        /// Inserts the given score into the XML file and updates the ordering.
        /// </summary>
        /// <param name="score">The score to be inserted.</param>
        void UpdateScoreBoard(XElement score);
    }
}
