﻿// <copyright file="ShopBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Classes
{
    using System;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer.Interfaces;

    /// <summary>
    /// A concrete shop builder using buttons as the grid elements.
    /// </summary>
    public class ShopBuilder : IShopBuilder
    {
        private IGameModel model;
        private Grid shop;
        private int cellsInColumn;
        private double buttonHeight;
        private double buttonWidth;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShopBuilder"/> class.
        /// </summary>
        /// <param name="model">The game model containing the elements displayed in the shop.</param>
        public ShopBuilder(IGameModel model)
        {
            this.model = model;
            this.cellsInColumn = 5;
            this.InitShop();
            this.buttonHeight = this.shop.Height / this.shop.RowDefinitions.Count * 0.6;
            this.buttonWidth = this.shop.Width / this.shop.ColumnDefinitions.Count * 0.6;
        }

        /// <inheritdoc/>
        public IShopBuilder AddExit()
        {
            Button exit = new Button();
            exit.Content = "Exit";
            exit.Foreground = Brushes.Blue;
            exit.Background = Brushes.Transparent;
            exit.BorderBrush = null;
            exit.MaxHeight = this.buttonHeight;
            exit.MaxWidth = this.buttonWidth / 3;

            this.shop.Children.Add(exit);
            Grid.SetRow(exit, (this.model.ItemsSoldInShop.Count % this.cellsInColumn) + 2);
            Grid.SetColumn(exit, (int)Math.Floor(this.model.ItemsSoldInShop.Count * 1.0 / this.cellsInColumn));

            return this;
        }

        /// <inheritdoc/>
        public IShopBuilder AddItems()
        {
            for (int i = 0; i < this.model.ItemsSoldInShop.Count; i++)
            {
                Button shopItem = new Button();
                shopItem.Content = this.model.ItemsSoldInShop[i].ToString();
                shopItem.Foreground = Brushes.Blue;
                shopItem.Background = Brushes.Transparent;
                shopItem.BorderBrush = null;
                shopItem.MaxHeight = this.buttonHeight;
                shopItem.MaxWidth = this.buttonWidth;
                shopItem.Tag = this.model.ItemsSoldInShop[i];

                this.shop.Children.Add(shopItem);
                Grid.SetRow(shopItem, (i % this.cellsInColumn) + 2);
                Grid.SetColumn(shopItem, (int)Math.Floor(i * 1.0 / this.cellsInColumn));
            }

            return this;
        }

        /// <inheritdoc/>
        public IShopBuilder AddCoinCounter()
        {
            Label coinCount = new Label();
            coinCount.Content = "Coins:\t" + this.model.Player.CoinCount;
            coinCount.Foreground = Brushes.BlueViolet;
            coinCount.HorizontalAlignment = HorizontalAlignment.Center;
            coinCount.VerticalAlignment = VerticalAlignment.Center;
            coinCount.FontSize = 13;
            coinCount.Name = "Coins";

            this.shop.Children.Add(coinCount);
            Grid.SetRow(coinCount, 1);
            Grid.SetColumn(coinCount, (this.shop.ColumnDefinitions.Count + 1) / 2);

            return this;
        }

        /// <inheritdoc/>
        public IShopBuilder AddTitle()
        {
            Label title = new Label();
            title.Content = "Shop";
            title.Foreground = Brushes.BlueViolet;
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.VerticalAlignment = VerticalAlignment.Center;
            title.FontFamily = new FontFamily("Papyrus");
            title.FontSize = 15;

            this.shop.Children.Add(title);
            Grid.SetRow(title, 1);
            Grid.SetColumn(title, (this.shop.ColumnDefinitions.Count - 1) / 2);

            return this;
        }

        /// <inheritdoc/>
        public Grid GetShop()
        {
            return this.shop;
        }

        private void InitShop()
        {
            this.shop = new Grid();
            for (int i = 0; i < this.cellsInColumn + 4; i++)
            {
                this.shop.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < (int)Math.Ceiling(this.model.ItemsSoldInShop.Count * 1.0 / this.cellsInColumn); i++)
            {
                this.shop.ColumnDefinitions.Add(new ColumnDefinition());
            }

            this.shop.Width = this.model.WindowWidth * 0.5;
            this.shop.Height = this.model.WindowHeight * 0.5;
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("StickyRickysAdventures.GameRenderer.Resources.ShopResources.Shop_Background.png");
            if (bmp.StreamSource != null)
            {
                ImageBrush ib = new ImageBrush(bmp);
                this.shop.Background = ib;
                bmp.EndInit();
            }
        }
    }
}
