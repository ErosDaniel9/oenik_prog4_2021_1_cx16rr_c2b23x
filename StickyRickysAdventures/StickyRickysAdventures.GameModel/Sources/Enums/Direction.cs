﻿// <copyright file="Direction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Enums
{
    /// <summary>
    /// A collection of different direction types.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// The character faces in the left direction.
        /// </summary>
        Left,

        /// <summary>
        /// The character faces in the right direction.
        /// </summary>
        Right,
    }
}
