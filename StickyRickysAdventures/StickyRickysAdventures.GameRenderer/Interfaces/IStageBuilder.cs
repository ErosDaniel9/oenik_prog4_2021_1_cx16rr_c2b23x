﻿// <copyright file="IStageBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Interfaces
{
    using System.Windows.Media;

    /// <summary>
    /// The methods and parameters required for rendering the gameplay.
    /// </summary>
    public interface IStageBuilder
    {
        /// <summary>
        /// Adds the player to the stage.
        /// </summary>
        /// <param name="pictureIndex">The index of the selected picture in our animation sequence.</param>
        /// <returns>The Builder.</returns>
        IStageBuilder AddPlayer(int pictureIndex);

        /// <summary>
        /// Adds enemies to the stage.
        /// </summary>
        /// <param name="pictureIndex">The index of the selected picture in our animation sequence.</param>
        /// <returns>The Builder.</returns>
        IStageBuilder AddEnemies(int pictureIndex);

        /// <summary>
        /// Adds platforms to the stage.
        /// </summary>
        /// <returns>The Builder.</returns>
        IStageBuilder AddPlatforms();

        /// <summary>
        /// Adds the vendor to the stage.
        /// </summary>
        /// <param name="pictureIndex">The index of the selected picture in our animation sequence.</param>
        /// <returns>The Builder.</returns>
        IStageBuilder AddVendor(int pictureIndex);

        /// <summary>
        /// Adds ladders to the stage.
        /// </summary>
        /// <returns>The Builder.</returns>
        IStageBuilder AddLadders();

        /// <summary>
        /// Adds a background to the stage.
        /// </summary>
        /// <returns>The Builder.</returns>
        IStageBuilder AddBackground();

        /// <summary>
        /// Adds bullets to the stage.
        /// </summary>
        /// <returns>The Builder.</returns>
        IStageBuilder AddBullets();

        /// <summary>
        /// Adds an exit to the stage.
        /// </summary>
        /// <returns>The Builder.</returns>
        IStageBuilder AddExit();

        /// <summary>
        /// Returns the rendered stage.
        /// </summary>
        /// <returns>The rendered stage.</returns>
        DrawingGroup GetStage();

        /// <summary>
        /// Adds enemy health bars to the stage.
        /// </summary>
        /// <returns>The Builder.</returns>
        public IStageBuilder AddEnemyHealthBars();

        /// <summary>
        /// Adds the player HUD to the stage.
        /// </summary>
        /// <returns>The Builder.</returns>
        public IStageBuilder AddHUD();

        /// <summary>
        /// Clears all cached data in the renderer.
        /// </summary>
        void ClearCache();
    }
}
