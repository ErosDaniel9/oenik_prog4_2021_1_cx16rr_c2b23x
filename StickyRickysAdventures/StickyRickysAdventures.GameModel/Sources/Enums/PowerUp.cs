﻿// <copyright file="PowerUp.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Enums
{
    /// <summary>
    /// Will contain all the powerups in the game. A placeholder right now.
    /// </summary>
    public enum PowerUp
    {
        /// <summary>
        /// Upgrades the melee attack damage of the consumer.
        /// </summary>
        MeleeAttackDamage,

        /// <summary>
        /// Upgrades the ranged attack damage of the consumer.
        /// </summary>
        RangedAttackDamage,

        /// <summary>
        /// Upgrades the attack speed of the consumer.
        /// </summary>
        AttackSpeed,

        /// <summary>
        /// Upgrades the movement speed of the consumer.
        /// </summary>
        MovementSpeed,

        /// <summary>
        /// Upgrades the current and maximum health of the consumer.
        /// </summary>
        Health,
    }
}
