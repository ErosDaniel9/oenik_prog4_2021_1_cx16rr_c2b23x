﻿// <copyright file="Bullet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Classes
{
    using System;
    using StickyRickysAdventures.GameModel.Sources.Enums;
    using StickyRickysAdventures.GameModel.Sources.Interfaces;

    /// <summary>
    /// A class representing the bullets shot by any character.
    /// </summary>
    public class Bullet : GameItem, IMover
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bullet"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the position rectangle.</param>
        /// <param name="y">The y coordinate of the position rectangle.</param>
        /// <param name="width">The width of the position rectangle.</param>
        /// <param name="height">The height of the position rectangle.</param>
        public Bullet(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
            this.MovementSpeed = 15;
        }

        /// <inheritdoc/>
        public float DX { get; set; }

        /// <inheritdoc/>
        public float DY { get; set; }

        /// <inheritdoc/>
        public Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets the CombatGameItem who shot the bullet.
        /// </summary>
        public ICombatGameItem Owner { get; set; }

        /// <inheritdoc/>
        public double MovementSpeed { get; set; }

        /// <summary>
        /// Gets owner type as a string.
        /// </summary>
        public string OwnerString
        {
            get
            {
                if (this.Owner is Player)
                {
                    return "player";
                }
                else if (this.Owner is Boss)
                {
                    return "boss";
                }
                else
                {
                    return "rangedEnemy";
                }
            }
        }
    }
}
