﻿// <copyright file="IFighter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Interfaces
{
    using System.Windows.Threading;
    using StickyRickysAdventures.GameModel.Sources.Enums;

    /// <summary>
    /// Methods and properties for all objects who can fight in the game.
    /// </summary>
    public interface IFighter
    {
        /// <summary>
        /// Gets or sets the maximum health of the fighter.
        /// </summary>
        int MaxHealth { get; set; }

        /// <summary>
        /// Gets or sets the current health of the fighter.
        /// </summary>
        int CurrentHealth { get; set; }

        /// <summary>
        /// Gets or sets the current melee damage of the fighter.
        /// </summary>
        int MeleeDamage { get; set; }

        /// <summary>
        /// Gets or sets the current ranged damage of the fighter.
        /// </summary>
        int RangedDamage { get; set; }

        /// <summary>
        /// Gets or sets the number of bullets left with the fighter.
        /// </summary>
        int BulletCount { get; set; }

        /// <summary>
        /// Gets or sets the current attack type of the fighter.
        /// </summary>
        AttackType CurrentAttackType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the character is attacking.
        /// </summary>
        bool IsAttacking { get; set; }

        /// <summary>
        /// Gets or sets the attack speed of the fighter.
        /// </summary>
        double AttackSpeed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the fighter can attack.
        /// </summary>
        bool CanAttack { get; set; }

        /// <summary>
        /// Gets or sets the attack timer of the fighter.
        /// </summary>
        DispatcherTimer AttackTimer { get; set; }
    }
}
