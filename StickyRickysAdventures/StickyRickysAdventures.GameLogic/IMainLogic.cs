﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameLogic
{
    using System.Windows.Input;

    /// <summary>
    /// Logic of the main game.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Gets a value indicating whether there is no enemy on the map.
        /// </summary>
        public bool CanContinue { get; }

        /// <summary>
        /// On call the player's character attacks.
        /// </summary>
        void PlayerAttack();

        /// <summary>
        /// Ticking.
        /// </summary>
        void OneTick();

        /// <summary>
        /// Modify the player's movements by releasing key.
        /// </summary>
        /// <param name="key">Released key.</param>
        void KeyUp(Key key);

        /// <summary>
        /// Modify the player's movements by pressing key.
        /// </summary>
        /// <param name="key">Released key.</param>
        void KeyDown(Key key);
    }
}
