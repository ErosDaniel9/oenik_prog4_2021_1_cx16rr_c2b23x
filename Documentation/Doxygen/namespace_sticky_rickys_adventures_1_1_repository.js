var namespace_sticky_rickys_adventures_1_1_repository =
[
    [ "GameStateRepository", "class_sticky_rickys_adventures_1_1_repository_1_1_game_state_repository.html", "class_sticky_rickys_adventures_1_1_repository_1_1_game_state_repository" ],
    [ "HighScoreRepository", "class_sticky_rickys_adventures_1_1_repository_1_1_high_score_repository.html", "class_sticky_rickys_adventures_1_1_repository_1_1_high_score_repository" ],
    [ "IGameStateRepository", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository.html", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository" ],
    [ "IHighScoreRepository", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_high_score_repository.html", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_high_score_repository" ]
];