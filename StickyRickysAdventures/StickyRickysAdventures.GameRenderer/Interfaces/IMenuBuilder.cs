﻿// <copyright file="IMenuBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Interfaces
{
    using System.Windows.Controls;

    /// <summary>
    /// Methods and parameters required to render the in-game menu.
    /// </summary>
    public interface IMenuBuilder
    {
        /// <summary>
        /// Adds buttons to the grid.
        /// </summary>
        /// <returns>The builder.</returns>
        public IMenuBuilder AddButtons();

        /// <summary>
        /// Initializes the menu grid as a full screen object with space for game name at the top and space for buttons at the bottom of the grid.
        /// </summary>
        /// <returns>The builder.</returns>
        public IMenuBuilder InitMenu();

        /// <summary>
        /// Adds title to the grid.
        /// </summary>
        /// <returns>The builder.</returns>
        public IMenuBuilder AddTitle();

        /// <summary>
        /// Returns the grid representing the menu.
        /// </summary>
        /// <returns>The grid representing the menu.</returns>
        public Grid GetMenu();
    }
}
