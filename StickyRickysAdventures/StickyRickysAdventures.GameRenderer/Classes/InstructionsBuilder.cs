﻿// <copyright file="InstructionsBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Classes
{
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer.Interfaces;

    /// <summary>
    /// A <see cref="IListWithExitBuilder"/> implementation for our how-to screen.
    /// </summary>
    public class InstructionsBuilder : IListWithExitBuilder
    {
        private const int INSTRUCTIONOFFSET = 3;
        private Grid screen;
        private string[] instructions;
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstructionsBuilder"/> class.
        /// </summary>
        /// <param name="model">The game model containing our window data.</param>
        public InstructionsBuilder(IGameModel model)
        {
            this.model = model;
            this.InitInstructions();
            this.InitScreen();
        }

        /// <inheritdoc/>
        public IListWithExitBuilder AddExit()
        {
            Button exit = new Button();
            exit.BorderBrush = null;
            exit.Content = "Exit to Main Menu";
            exit.Foreground = Brushes.White;
            exit.FontStyle = FontStyles.Italic;
            exit.Background = Brushes.DarkGreen;
            exit.MaxHeight = this.screen.Height / (this.instructions.Length + (2 * INSTRUCTIONOFFSET));
            exit.MaxWidth = this.screen.Width / 3 * 0.4;
            exit.BorderBrush = Brushes.Brown;
            exit.BorderThickness = new Thickness(exit.MaxHeight * 0.05);

            this.screen.Children.Add(exit);
            Grid.SetColumn(exit, 1);
            Grid.SetRow(exit, this.instructions.Length + INSTRUCTIONOFFSET + 1);
            return this;
        }

        /// <inheritdoc/>
        public IListWithExitBuilder AddListElements()
        {
            int i = 0;
            while (i < this.instructions.Length)
            {
                Label instruction = new Label();
                instruction.Content = this.instructions[i];
                instruction.Foreground = Brushes.White;
                instruction.Background = Brushes.Transparent;
                instruction.FontStyle = FontStyles.Italic;
                instruction.FontSize = 15;
                instruction.HorizontalAlignment = HorizontalAlignment.Center;
                instruction.VerticalAlignment = VerticalAlignment.Center;

                this.screen.Children.Add(instruction);
                Grid.SetColumn(instruction, 1);
                Grid.SetRow(instruction, INSTRUCTIONOFFSET + i);
                i++;
            }

            return this;
        }

        /// <inheritdoc/>
        public IListWithExitBuilder AddTitle()
        {
            Label title = new Label();
            title.Content = "Instructions";
            title.Foreground = Brushes.AliceBlue;
            title.Background = Brushes.Transparent;
            title.FontFamily = new FontFamily("Papyrus");
            title.FontSize = 20;
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.VerticalAlignment = VerticalAlignment.Center;

            this.screen.Children.Add(title);
            Grid.SetColumn(title, 1);
            Grid.SetRow(title, 1);

            return this;
        }

        /// <inheritdoc/>
        public Grid GetHighScoreScreen()
        {
            return this.screen;
        }

        private void InitInstructions()
        {
            this.instructions = new string[]
            {
                "Swap weapon: Q",
                "Move left/right: A-D",
                "Move up/down ladder: W-S",
                "Jump: Space",
                "Attack: Click",
                "Sprint: Left Shift",
                "Consume Potion: G",
                "Open/Close shop: E",
                "Open/Close menu: Esc",
                "The goal of the game is to finish the last map.",
                "You can only go to the next map when you eliminated all enemies on the level.",
                "You get coins when you defeat an enemy, use them to buy upgrades at the vendor.",
                "For more information, look at the game manual.",
            };
        }

        private void InitScreen()
        {
            this.screen = new Grid();
            for (int i = 0; i < this.instructions.Length + (INSTRUCTIONOFFSET * 2); i++)
            {
                this.screen.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < 3; i++)
            {
                this.screen.ColumnDefinitions.Add(new ColumnDefinition());
            }

            this.screen.Width = this.model.WindowWidth;
            this.screen.Height = this.model.WindowHeight;

            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("StickyRickysAdventures.GameRenderer.Resources.MenuResources.Main_Menu_Background.png");
            if (bmp.StreamSource != null)
            {
                ImageBrush ib = new ImageBrush(bmp);
                this.screen.Background = ib;
                bmp.EndInit();
            }
        }
    }
}
