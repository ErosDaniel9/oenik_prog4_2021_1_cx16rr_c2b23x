﻿// <copyright file="ICombatGameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Interfaces
{
    /// <summary>
    /// Complex GameItem that can be moved and able to attack.
    /// </summary>
    public interface ICombatGameItem : IGameItem, IMover, IFighter
    {
    }
}
