﻿// <copyright file="ISaveGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameLogic
{
    /// <summary>
    /// Interface for savegame logic.
    /// </summary>
    public interface ISaveGameLogic
    {
        /// <summary>
        /// Saves the game.
        /// </summary>
        void SaveGame();
    }
}
