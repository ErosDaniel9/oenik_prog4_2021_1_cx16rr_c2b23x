﻿// <copyright file="ShopItemWIthCost.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Structs
{
    using System;
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// A struct containing a consumable enum and a cost int.
    /// </summary>
    public struct ShopItemWithCost : IEquatable<ShopItemWithCost>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShopItemWithCost"/> struct.
        /// </summary>
        /// <param name="shopItem">The item sold in the shop.</param>
        /// <param name="cost">The cost of the item.</param>
        public ShopItemWithCost(Enum shopItem, int cost)
        {
            this.ShopItem = shopItem;
            this.Cost = cost;
        }

        /// <summary>
        /// Gets or sets the consumable.
        /// </summary>
        public Enum ShopItem { get; set; }

        /// <summary>
        /// Gets or sets the cost.
        /// </summary>
        public int Cost { get; set; }

        /// <summary>
        /// Overrides the equivalence operator.
        /// </summary>
        /// <param name="left">The object on the left of the equation.</param>
        /// <param name="right">The object on the right of the equation.</param>
        /// <returns>A value indicating whether the two values are equal.</returns>
        public static bool operator ==(ShopItemWithCost left, ShopItemWithCost right) => left.Equals(right);

        /// <summary>
        /// Overrides the inequivalence operator.
        /// </summary>
        /// <param name="left">The object on the left of the equation.</param>
        /// <param name="right">The object on the right of the equation.</param>
        /// <returns>A value indicating whether the two values are not equal.</returns>
        public static bool operator !=(ShopItemWithCost left, ShopItemWithCost right) => !left.Equals(right);

        /// <inheritdoc/>
        public override string ToString()
        {
            string cleanedUpName = string.Concat(this.ShopItem.ToString().Select(x => char.IsUpper(x) ? " " + x : x.ToString()));
            return string.Format(CultureInfo.InvariantCulture, "{0, -40}\tcost: {1, 2}", cleanedUpName, this.Cost.ToString(CultureInfo.InvariantCulture));
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <inheritdoc/>
        public bool Equals(ShopItemWithCost other)
        {
            return base.Equals(other);
        }
    }
}
