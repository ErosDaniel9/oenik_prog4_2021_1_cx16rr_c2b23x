﻿// <copyright file="InGameMenuController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Controller
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using StickyRickysAdventures.GameLogic;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer;
    using StickyRickysAdventures.GameRenderer.Classes;

    /// <summary>
    /// The controller for the main menu.
    /// </summary>
    public class InGameMenuController : FrameworkElement
    {
        private IGameModel gameModel;
        private MainRenderer gameRenderer;
        private ISaveGameLogic saveGameLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="InGameMenuController"/> class.
        /// </summary>
        /// <param name="gameModel">The game model containing the game data.</param>
        public InGameMenuController(IGameModel gameModel)
        {
            this.Loaded += this.InitController;
            this.gameModel = gameModel;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            bool playerIsDead = this.gameModel.Player.CurrentHealth <= 0;
            const int DEATHPICTURE = 6;
            const int ALIVEPICTURE = 1;
            if (this.gameRenderer != null)
            {
                int currentPic = playerIsDead ? DEATHPICTURE : ALIVEPICTURE;
                drawingContext?.DrawDrawing(this.gameRenderer.GetGameplay(currentPic));
            }
        }

        private void InitController(object sender, RoutedEventArgs e)
        {
            MainWindow win = Application.Current.MainWindow as MainWindow;
            if (win != null)
            {
                Grid menu = this.InitializeComponents(win);
                this.SubscribeButtonsToLogic(menu);
            }

            this.InvalidateVisual();
        }

        private Grid InitializeComponents(MainWindow win)
        {
            this.InitializeRenderer();
            this.InitializeLogic();
            return this.InitializeMenuGrid(win);
        }


        private void InitializeRenderer()
        {
            string[] buttonNames = new string[] { "Resume", "Save Game", "Exit to Main Menu" };
            this.gameRenderer = new MainRenderer(new StageBuilder(this.gameModel), new InGameMenuBuilder(this.gameModel, buttonNames), null, null, null);
        }

        private Grid InitializeMenuGrid(MainWindow win)
        {
            Grid menu = this.gameRenderer.GetMenu();
            win.Display.Children.Add(menu);
            win.RegisterName("Menu", menu);
            return menu;
        }

        private void InitializeLogic() => this.saveGameLogic = new SaveGameLogic(this.gameModel);

        private void SubscribeButtonsToLogic(Grid menu)
        {
            if (menu.Children.Count != 0)
            {
                foreach (var child in menu.Children)
                {
                    if (child is Button)
                    {
                        Button menuButton = child as Button;
                        if (menuButton.Content.Equals("Save Game"))
                        {
                            menuButton.Click -= this.SaveGame;
                            menuButton.Click += this.SaveGame;
                        }
                    }
                }
            }
        }

        private void SaveGame(object sender, RoutedEventArgs e)
        {
            this.saveGameLogic.SaveGame();
        }
    }
}
