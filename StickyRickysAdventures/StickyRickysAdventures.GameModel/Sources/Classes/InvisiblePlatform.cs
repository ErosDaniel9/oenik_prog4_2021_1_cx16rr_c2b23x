﻿// <copyright file="InvisiblePlatform.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Classes
{
    /// <summary>
    /// A platform that is invisible.
    /// </summary>
    public class InvisiblePlatform : Platform
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvisiblePlatform"/> class.
        /// </summary>
        /// <param name="x">The x coord.</param>
        /// <param name="y">The y coord.</param>
        /// <param name="width">The width of the rectangle.</param>
        /// <param name="height">The height of the rectangle.</param>
        public InvisiblePlatform(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
        }
    }
}
