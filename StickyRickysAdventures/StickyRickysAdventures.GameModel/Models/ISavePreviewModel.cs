﻿// <copyright file="ISavePreviewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Models
{
    using System;

    /// <summary>
    /// All the properties contained in a non-memory intensive preview of a save.
    /// </summary>
    public interface ISavePreviewModel : IFormattable
    {
        /// <summary>
        /// Gets or sets the Id of the save.
        /// </summary>
        public int SaveID { get; set; }

        /// <summary>
        /// Gets or sets the score of the player in the save.
        /// </summary>
        public int PlayerScore { get; set; }

        /// <summary>
        /// Gets or sets the current hp of the player in the save.
        /// </summary>
        public int PlayerHp { get; set; }

        /// <summary>
        /// Gets or sets the current map in the save.
        /// </summary>
        public string CurrentMap { get; set; }
    }
}
