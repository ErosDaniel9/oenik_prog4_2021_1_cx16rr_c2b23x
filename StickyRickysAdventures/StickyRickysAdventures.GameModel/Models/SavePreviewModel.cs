﻿// <copyright file="SavePreviewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Models
{
    using System;

    /// <summary>
    /// Concrete implementation of the <see cref="ISavePreviewModel"/> interface.
    /// </summary>
    public class SavePreviewModel : ISavePreviewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SavePreviewModel"/> class.
        /// </summary>
        /// <param name="saveID">The Id of the save.</param>
        /// <param name="playerScore">The score of the player in the save.</param>
        /// <param name="playerHp">The current hp of the player in the save.</param>
        /// <param name="currentMap">The map the player is on in the save.</param>
        public SavePreviewModel(int saveID, int playerScore, int playerHp, string currentMap)
        {
            this.SaveID = saveID;
            this.PlayerScore = playerScore;
            this.PlayerHp = playerHp;
            this.CurrentMap = currentMap;
        }

        /// <inheritdoc/>
        public int SaveID { get; set; }

        /// <inheritdoc/>
        public int PlayerScore { get; set; }

        /// <inheritdoc/>
        public int PlayerHp { get; set; }

        /// <inheritdoc/>
        public string CurrentMap { get; set; }

        /// <inheritdoc/>
        public string ToString(string format, IFormatProvider formatProvider = null)
        {
            return $"Id:{this.SaveID}\tScore:{this.PlayerScore}\tHP:{this.PlayerHp}\tMap:{this.CurrentMap}";
        }
    }
}
