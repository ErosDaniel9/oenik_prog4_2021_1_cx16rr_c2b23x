﻿// <copyright file="ShopController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Controller
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using StickyRickysAdventures.GameLogic;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameModel.Sources.Structs;
    using StickyRickysAdventures.GameRenderer;
    using StickyRickysAdventures.GameRenderer.Classes;

    /// <summary>
    /// The controller object for the shop.
    /// </summary>
    public class ShopController : FrameworkElement
    {
        private IGameModel model;
        private MainRenderer renderer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShopController"/> class.
        /// </summary>
        /// <param name="model">The model containing shop data.</param>
        public ShopController(IGameModel model)
        {
            this.model = model;
            this.Loaded += this.InitModel;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                DrawingGroup stage = this.renderer.GetGameplay(1) as DrawingGroup;
                if (stage != null)
                {
                    stage.Opacity = 0.7;
                    drawingContext?.DrawDrawing(stage);
                }
            }
        }

        private void InitModel(object sender, RoutedEventArgs e)
        {
            MainWindow win = Application.Current.MainWindow as MainWindow;
            if (win != null)
            {
                this.renderer = new MainRenderer(new StageBuilder(this.model), null, new ShopBuilder(this.model), null, null);

                Grid shop = this.renderer.GetShop();

                if (shop != null)
                {
                    win.Display.Children.Add(shop);
                    win.RegisterName("Shop", shop);
                    foreach (var child in shop.Children)
                    {
                        if (child is Button && !(child as Button).Content.Equals("Exit"))
                        {
                            (child as Button).Click += this.BuyItem;
                        }
                    }
                }
            }

            this.InvalidateVisual();
        }

        private void BuyItem(object sender, RoutedEventArgs e)
        {
            ShopLogic.BuyUpgrades(this.model, (ShopItemWithCost)(sender as Button).Tag);
            this.UpdateCoinCount();
        }

        private void UpdateCoinCount()
        {
            MainWindow win = Window.GetWindow(this) as MainWindow;
            Grid shop = win.FindName("Shop") as Grid;
            if (shop != null)
            {
                foreach (var child in shop.Children)
                {
                    if (child is Label && (child as Label).Name.Equals("Coins", StringComparison.Ordinal))
                    {
                        (child as Label).Content = "Coins:\t" + this.model.Player.CoinCount;
                    }
                }
            }
        }
    }
}
