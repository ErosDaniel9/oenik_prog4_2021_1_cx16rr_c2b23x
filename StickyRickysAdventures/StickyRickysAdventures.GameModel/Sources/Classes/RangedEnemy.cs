﻿// <copyright file="RangedEnemy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Classes
{
    using System;
    using System.Windows;
    using StickyRickysAdventures.GameModel.Sources.Enums;

    /// <summary>
    /// Class representing the basic ranged enemy.
    /// </summary>
    public sealed class RangedEnemy : CombatGameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RangedEnemy"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the position rectangle.</param>
        /// <param name="y">The y coordinate of the position rectangle.</param>
        /// <param name="width">The width of the position rectangle.</param>
        /// <param name="height">The height of the position rectangle.</param>
        public RangedEnemy(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
            this.MaxHealth = 60;
            this.CurrentHealth = 60;
            this.MeleeDamage = 0;
            this.RangedDamage = 15;
            this.CurrentAttackType = AttackType.Ranged;
            this.BulletCount = 20000; // So it doesn't run out of bullets.
            this.MovementSpeed = 1;
            this.AttackSpeed = 1;
        }
    }
}
