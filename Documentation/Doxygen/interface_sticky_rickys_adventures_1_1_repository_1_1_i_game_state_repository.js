var interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository =
[
    [ "DeleteSave", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository.html#a5066cda1400b6977493f0d3bc25b4e5c", null ],
    [ "GetAllSaves", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository.html#a2c1434caf39cc900d49a45eb6d031b4c", null ],
    [ "GetOneSave", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository.html#ab92e3f32ba85ed56cbcc0854b561fc46", null ],
    [ "SaveGame", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository.html#a48ebaf29d4684747fe10f2c5bbc177f6", null ]
];