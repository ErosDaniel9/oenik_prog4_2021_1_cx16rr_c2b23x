﻿// <copyright file="Consumable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Enums
{
    /// <summary>
    /// Will contain all consumables in the game. A placeholder right now.
    /// </summary>
    public enum Consumable
    {
        /// <summary>
        /// Refills a set amount of health of the consumer.
        /// </summary>
        HealthPotion,
    }
}
