﻿// <copyright file="IShopBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Interfaces
{
    using System.Windows.Controls;

    /// <summary>
    /// The methods and parameters required for rendering the in-game shop.
    /// </summary>
    public interface IShopBuilder
    {
        /// <summary>
        /// Adds player coin counter to the shop.
        /// </summary>
        /// <returns>The builder.</returns>
        public IShopBuilder AddCoinCounter();

        /// <summary>
        /// Add the sold items to the shop.
        /// </summary>
        /// <returns>The builder.</returns>
        public IShopBuilder AddItems();

        /// <summary>
        /// Adds a title to the shop.
        /// </summary>
        /// <returns>The builder.</returns>
        public IShopBuilder AddTitle();

        /// <summary>
        /// Adds an exit point to the shop.
        /// </summary>
        /// <returns>The builder.</returns>
        public IShopBuilder AddExit();

        /// <summary>
        /// Returns the shop interface.
        /// </summary>
        /// <returns>A grid representing the shop interface.</returns>
        public Grid GetShop();
    }
}
