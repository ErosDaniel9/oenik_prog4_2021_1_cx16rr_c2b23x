﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Load Game Logic initializes it outside of constructor.")]
[assembly: SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "Logic uses it as a matrix.")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Logic uses it as a matrix.")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Load Game Logic initializes it outside of constructor.")]
