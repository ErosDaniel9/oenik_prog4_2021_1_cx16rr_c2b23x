var interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item =
[
    [ "GetX", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item.html#ae44ff433e60889dabe040d3d92d4296e", null ],
    [ "GetY", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item.html#a1c343c80e46551c4cbcc587f0605003c", null ],
    [ "SetX", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item.html#a7e3c28ad7cab0063e9eec4d3f6021875", null ],
    [ "SetY", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item.html#ae795a0caed050a8650c0fd6cea748221", null ],
    [ "Position", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item.html#ac11bf207a309b7d340b2648be027180f", null ]
];