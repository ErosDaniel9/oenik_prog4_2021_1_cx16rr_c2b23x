﻿// <copyright file="MainGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Models
{
    using System.Collections.Generic;
    using System.Windows;
    using StickyRickysAdventures.GameModel.Sources.Classes;
    using StickyRickysAdventures.GameModel.Sources.Enums;
    using StickyRickysAdventures.GameModel.Sources.Structs;

    /// <summary>
    /// A concrete implementation of the IGameModel interface. Contains all displayable necessary for the gameplay.
    /// </summary>
    public class MainGameModel : IGameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainGameModel"/> class.
        /// </summary>
        /// <param name="windowWidth">The width of the window.</param>
        /// <param name="windowHeight">The height of the window.</param>
        public MainGameModel(int windowWidth, int windowHeight)
        {
            this.WindowWidth = windowWidth;
            this.WindowHeight = windowHeight;
            this.Player = new Player(0, 0, this.TileWidth, this.TileHeight);
            this.Enemies = new List<CombatGameItem>();
            this.Bullets = new List<Bullet>();
            this.InitShopItems();
            this.NextMapExists = true;
            this.CurrentMap = "00.txt";
        }

        /// <inheritdoc/>
        public bool MapChanging { get; set; }

        /// <inheritdoc/>
        public int WindowWidth { get; set; }

        /// <inheritdoc/>
        public int WindowHeight { get; set; }

        /// <inheritdoc/>
        public Player Player { get; set; }

        /// <inheritdoc/>
        public IList<CombatGameItem> Enemies { get; set; }

        /// <inheritdoc/>
        public Rect Vendor { get; set; }

        /// <inheritdoc/>
        public IList<ShopItemWithCost> ItemsSoldInShop { get; set; }

        /// <inheritdoc/>
        public IList<Bullet> Bullets { get; set; }

        /// <inheritdoc/>
        public int TileHeight { get; set; }

        /// <inheritdoc/>
        public int TileWidth { get; set; }

        /// <inheritdoc/>
        public Rect EndPoint { get; set; }

        /// <inheritdoc/>
        public Platform[,] Platforms { get; set; }

        /// <inheritdoc/>
        public int TickSpeed { get; set; }

        /// <inheritdoc/>
        public Boss Boss { get; set; }

        /// <inheritdoc/>
        public string CurrentMap { get; set; }

        /// <inheritdoc/>
        public bool NextMapExists { get; set; }

        private void InitShopItems()
        {
            this.ItemsSoldInShop = new List<ShopItemWithCost>();
            this.ItemsSoldInShop.Add(new ShopItemWithCost(PowerUp.AttackSpeed, 100));
            this.ItemsSoldInShop.Add(new ShopItemWithCost(PowerUp.Health, 70));
            this.ItemsSoldInShop.Add(new ShopItemWithCost(PowerUp.MeleeAttackDamage, 50));
            this.ItemsSoldInShop.Add(new ShopItemWithCost(PowerUp.MovementSpeed, 80));
            this.ItemsSoldInShop.Add(new ShopItemWithCost(PowerUp.RangedAttackDamage, 80));
            this.ItemsSoldInShop.Add(new ShopItemWithCost(Consumable.HealthPotion, 60));
            this.ItemsSoldInShop.Add(new ShopItemWithCost(Service.RefillBullets, 60));
        }
    }
}
