﻿// <copyright file="GameController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Controller
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using StickyRickysAdventures.GameLogic;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer;
    using StickyRickysAdventures.GameRenderer.Classes;
    using StickyRickysAdventures.Repository;

    /// <summary>
    /// The main gameplay controller.
    /// </summary>
    public class GameController : FrameworkElement
    {
        private const int RenderTreshold = 1000 / 20;

        private IMainLogic logic;
        private ISaveGameLogic saveLogic;
        private MainRenderer renderer;
        private IGameModel model;
        private DispatcherTimer mainTimer;
        private int picToAnimate = 1;
        private Grid deathScreen;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameController"/> class.
        /// </summary>
        /// <param name="model">The gameModel containing the level data.</param>
        public GameController(IGameModel model)
        {
            this.model = model == null ? new MainGameModel((int)this.ActualWidth, (int)this.ActualHeight) : model;
            this.Loaded += this.InitControl;
            this.Unloaded += this.UnloadControl;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                int currentPic = this.model.Player.CurrentHealth <= 0 ? 6 : this.picToAnimate / (RenderTreshold / this.model.TickSpeed);
                drawingContext?.DrawDrawing(this.renderer.GetGameplay(currentPic));
            }
        }

        private void InitControl(object sender, RoutedEventArgs e)
        {
            MainWindow win = Application.Current.MainWindow as MainWindow;
            this.model.TickSpeed = 16;

            if (win != null)
            {
                this.logic = new MainLogic(this.model);
                this.saveLogic = new SaveGameLogic(this.model);
                this.renderer = new MainRenderer(new StageBuilder(this.model), null, null, null, new NameInputBuilder(this.model));

                win.KeyDown -= this.ChangePlayerVelocity;
                win.KeyDown += this.ChangePlayerVelocity;
                win.KeyUp -= this.ResetPlayerVelocity;
                win.KeyUp += this.ResetPlayerVelocity;
                win.MouseDown -= this.PlayerAttack;
                win.MouseDown += this.PlayerAttack;

                this.deathScreen = this.renderer.GetDeathScreen();
                this.SubSaveScoreToConfirm(this.deathScreen);
                this.deathScreen.Visibility = Visibility.Hidden;
                win.Display.Children.Add(this.deathScreen);
                win.RegisterName("DeathScreen", this.deathScreen);

                this.mainTimer = new DispatcherTimer();
                this.mainTimer.Interval = TimeSpan.FromMilliseconds(this.model.TickSpeed);
                this.mainTimer.Tick += this.RunGame;
                this.mainTimer.Start();
            }

            this.InvalidateVisual();
        }

        private void UnloadControl(object sender, RoutedEventArgs e)
        {
            Window win = Application.Current.MainWindow;
            win.KeyDown -= this.ChangePlayerVelocity;
            win.KeyUp -= this.ResetPlayerVelocity;
            win.MouseDown -= this.PlayerAttack;
            this.mainTimer.Tick -= this.RunGame;
            this.mainTimer.Stop();
        }

        private void PlayerAttack(object sender, MouseButtonEventArgs e)
        {
            this.logic.PlayerAttack();
        }

        private void ResetPlayerVelocity(object sender, KeyEventArgs e)
        {
            this.logic.KeyUp(e.Key);
        }

        private void RunGame(object sender, EventArgs e)
        {
            if (this.model.Player.CurrentHealth <= 0 &&
                this.picToAnimate / (RenderTreshold / this.model.TickSpeed) == 6)
            {
                this.deathScreen.Visibility = Visibility.Visible;
                this.UnloadControl(sender, null);
            }
            else if (!this.model.NextMapExists && this.model.Enemies.Count == 0)
            {
                this.ChangeToSuccessScreen();
                this.deathScreen.Visibility = Visibility.Visible;
                this.UnloadControl(sender, null);
            }

            this.logic.OneTick();
            this.InvalidateVisual();
            this.picToAnimate++;
            if (this.picToAnimate == 7 * (RenderTreshold / this.model.TickSpeed))
            {
                this.picToAnimate = 1;
            }
        }

        private void ChangeToSuccessScreen()
        {
            foreach (var child in this.deathScreen.Children)
            {
                if (child is Label && (child as Label).Tag.Equals("Message"))
                {
                    (child as Label).Content = "You won! Please enter your name...";
                    (child as Label).Foreground = Brushes.Green;
                }
            }
        }

        private void ChangePlayerVelocity(object sender, KeyEventArgs e)
        {
            this.logic.KeyDown(e.Key);
        }

        private void SubSaveScoreToConfirm(Grid grid)
        {
            foreach (var child in grid.Children)
            {
                if (child is Button && (child as Button).Content.Equals("Confirm"))
                {
                    (child as Button).Click += this.SaveScore;
                }
            }
        }

        private void SaveScore(object sender, RoutedEventArgs e)
        {
            string name = string.Empty;
            foreach (var child in this.deathScreen.Children)
            {
                if (child is TextBox)
                {
                    name = (child as TextBox).Text;
                }
            }

            HighscoreLogic.NewHighscore(new HighScoreRepository(), name, this.model.Player.Score);
        }
    }
}
