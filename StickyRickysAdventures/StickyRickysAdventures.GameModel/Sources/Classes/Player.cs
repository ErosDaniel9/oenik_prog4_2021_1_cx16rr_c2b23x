﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Classes
{
    using StickyRickysAdventures.GameModel.Sources.Enums;

    /// <summary>
    /// A data object containing all the properties of the player character.
    /// </summary>
    public class Player : CombatGameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the position rectangle.</param>
        /// <param name="y">The y coordinate of the position rectangle.</param>
        /// <param name="width">The width of the position rectangle.</param>
        /// <param name="height">The height of the position rectangle.</param>
        public Player(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
            this.MaxHealth = 100;
            this.CurrentHealth = 100;
            this.MeleeDamage = 20;
            this.RangedDamage = 12;
            this.CurrentAttackType = AttackType.Melee;
            this.BulletCount = 10;
            this.MovementSpeed = 10;
            this.AttackSpeed = 1;
            this.HealthPotionCount = 3;
            this.CoinCount = 0;
            this.Sprinting = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the player is falling.
        /// </summary>
        public bool IsFalling { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the player is jumping.
        /// </summary>
        public bool IsJumping { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the player is climbing a ladder.
        /// </summary>
        public bool IsClimbing { get; set; }

        /// <summary>
        /// Gets or sets the number of coins the player has.
        /// </summary>
        public int CoinCount { get; set; }

        /// <summary>
        /// Gets or sets the number of heatlh potions the player has.
        /// </summary>
        public int HealthPotionCount { get; set; }

        /// <summary>
        /// Gets or sets the score of the player.
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the player is sprinting or not.
        /// </summary>
        public bool Sprinting { get; set; }
    }
}
