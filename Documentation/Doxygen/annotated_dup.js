var annotated_dup =
[
    [ "StickyRickysAdventures", "namespace_sticky_rickys_adventures.html", [
      [ "Controller", "namespace_sticky_rickys_adventures_1_1_controller.html", [
        [ "GameController", "class_sticky_rickys_adventures_1_1_controller_1_1_game_controller.html", "class_sticky_rickys_adventures_1_1_controller_1_1_game_controller" ],
        [ "HighScoreController", "class_sticky_rickys_adventures_1_1_controller_1_1_high_score_controller.html", "class_sticky_rickys_adventures_1_1_controller_1_1_high_score_controller" ],
        [ "InGameMenuController", "class_sticky_rickys_adventures_1_1_controller_1_1_in_game_menu_controller.html", "class_sticky_rickys_adventures_1_1_controller_1_1_in_game_menu_controller" ],
        [ "LoadGameController", "class_sticky_rickys_adventures_1_1_controller_1_1_load_game_controller.html", "class_sticky_rickys_adventures_1_1_controller_1_1_load_game_controller" ],
        [ "MainMenuController", "class_sticky_rickys_adventures_1_1_controller_1_1_main_menu_controller.html", "class_sticky_rickys_adventures_1_1_controller_1_1_main_menu_controller" ],
        [ "MetaController", "class_sticky_rickys_adventures_1_1_controller_1_1_meta_controller.html", "class_sticky_rickys_adventures_1_1_controller_1_1_meta_controller" ],
        [ "ShopController", "class_sticky_rickys_adventures_1_1_controller_1_1_shop_controller.html", "class_sticky_rickys_adventures_1_1_controller_1_1_shop_controller" ]
      ] ],
      [ "GameLogic", "namespace_sticky_rickys_adventures_1_1_game_logic.html", [
        [ "HighscoreLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_highscore_logic.html", "class_sticky_rickys_adventures_1_1_game_logic_1_1_highscore_logic" ],
        [ "IMainLogic", "interface_sticky_rickys_adventures_1_1_game_logic_1_1_i_main_logic.html", "interface_sticky_rickys_adventures_1_1_game_logic_1_1_i_main_logic" ],
        [ "ISaveGameLogic", "interface_sticky_rickys_adventures_1_1_game_logic_1_1_i_save_game_logic.html", "interface_sticky_rickys_adventures_1_1_game_logic_1_1_i_save_game_logic" ],
        [ "LoadMapLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_load_map_logic.html", "class_sticky_rickys_adventures_1_1_game_logic_1_1_load_map_logic" ],
        [ "MainLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_main_logic.html", "class_sticky_rickys_adventures_1_1_game_logic_1_1_main_logic" ],
        [ "SaveGameLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_save_game_logic.html", "class_sticky_rickys_adventures_1_1_game_logic_1_1_save_game_logic" ],
        [ "ShopLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_shop_logic.html", "class_sticky_rickys_adventures_1_1_game_logic_1_1_shop_logic" ]
      ] ],
      [ "GameModel", "namespace_sticky_rickys_adventures_1_1_game_model.html", [
        [ "Models", "namespace_sticky_rickys_adventures_1_1_game_model_1_1_models.html", [
          [ "GameModel", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_game_model.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_game_model" ],
          [ "IGameModel", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model" ],
          [ "ISavePreviewModel", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_save_preview_model.html", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_save_preview_model" ],
          [ "SavePreviewModel", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model" ]
        ] ],
        [ "Sources", "namespace_sticky_rickys_adventures_1_1_game_model_1_1_sources.html", [
          [ "Classes", "namespace_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes.html", [
            [ "Boss", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_boss.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_boss" ],
            [ "Bullet", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet" ],
            [ "CombatGameItem", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_combat_game_item.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_combat_game_item" ],
            [ "InteractablePlatform", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_interactable_platform.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_interactable_platform" ],
            [ "MeleeEnemy", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_melee_enemy.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_melee_enemy" ],
            [ "Platform", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_platform.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_platform" ],
            [ "Player", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_player.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_player" ],
            [ "RangedEnemy", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_ranged_enemy.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_ranged_enemy" ]
          ] ],
          [ "Interfaces", "namespace_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces.html", [
            [ "ICombatGameItem", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_combat_game_item.html", null ],
            [ "IFighter", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter" ],
            [ "IGameItem", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item.html", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item" ],
            [ "IMover", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_mover.html", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_mover" ]
          ] ],
          [ "Structs", "namespace_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_structs.html", [
            [ "Score", "struct_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_structs_1_1_score.html", "struct_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_structs_1_1_score" ],
            [ "ShopItemWithCost", "struct_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_structs_1_1_shop_item_with_cost.html", "struct_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_structs_1_1_shop_item_with_cost" ]
          ] ],
          [ "GameItem", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_game_item.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_game_item" ],
          [ "StaticGameItem", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_static_game_item.html", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_static_game_item" ]
        ] ]
      ] ],
      [ "GameRenderer", "namespace_sticky_rickys_adventures_1_1_game_renderer.html", [
        [ "Classes", "namespace_sticky_rickys_adventures_1_1_game_renderer_1_1_classes.html", [
          [ "HighScoreBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_high_score_builder.html", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_high_score_builder" ],
          [ "InGameMenuBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_in_game_menu_builder.html", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_in_game_menu_builder" ],
          [ "LoadMenuBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_load_menu_builder.html", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_load_menu_builder" ],
          [ "MainMenuBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_main_menu_builder.html", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_main_menu_builder" ],
          [ "NameInputBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_name_input_builder.html", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_name_input_builder" ],
          [ "ShopBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_shop_builder.html", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_shop_builder" ],
          [ "StageBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_stage_builder.html", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_stage_builder" ]
        ] ],
        [ "Interfaces", "namespace_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces.html", [
          [ "IHighScoreBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_high_score_builder.html", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_high_score_builder" ],
          [ "IInputGridBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_input_grid_builder.html", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_input_grid_builder" ],
          [ "IMenuBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_menu_builder.html", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_menu_builder" ],
          [ "IShopBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_shop_builder.html", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_shop_builder" ],
          [ "IStageBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_stage_builder.html", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_stage_builder" ]
        ] ],
        [ "MainRenderer", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_main_renderer.html", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_main_renderer" ]
      ] ],
      [ "Logic", "namespace_sticky_rickys_adventures_1_1_logic.html", [
        [ "Test", "namespace_sticky_rickys_adventures_1_1_logic_1_1_test.html", [
          [ "AttackTests", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_attack_tests.html", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_attack_tests" ],
          [ "EnemyMovementTests", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_enemy_movement_tests.html", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_enemy_movement_tests" ],
          [ "LoadMapTests", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_load_map_tests.html", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_load_map_tests" ],
          [ "PlayerMovementTests", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_player_movement_tests.html", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_player_movement_tests" ],
          [ "Setups", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_setups.html", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_setups" ]
        ] ]
      ] ],
      [ "Repository", "namespace_sticky_rickys_adventures_1_1_repository.html", [
        [ "GameStateRepository", "class_sticky_rickys_adventures_1_1_repository_1_1_game_state_repository.html", "class_sticky_rickys_adventures_1_1_repository_1_1_game_state_repository" ],
        [ "HighScoreRepository", "class_sticky_rickys_adventures_1_1_repository_1_1_high_score_repository.html", "class_sticky_rickys_adventures_1_1_repository_1_1_high_score_repository" ],
        [ "IGameStateRepository", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository.html", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository" ],
        [ "IHighScoreRepository", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_high_score_repository.html", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_high_score_repository" ]
      ] ],
      [ "App", "class_sticky_rickys_adventures_1_1_app.html", "class_sticky_rickys_adventures_1_1_app" ],
      [ "MainWindow", "class_sticky_rickys_adventures_1_1_main_window.html", "class_sticky_rickys_adventures_1_1_main_window" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];