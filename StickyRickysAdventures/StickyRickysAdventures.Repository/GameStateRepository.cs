﻿// <copyright file="GameStateRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;

[assembly:CLSCompliant(false)]

namespace StickyRickysAdventures.Repository
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// The concrete implementation of our IGameStateRepository.
    /// </summary>
    public class GameStateRepository : IGameStateRepository
    {
        private const string FILENAME = "savefile.xml";

        /// <inheritdoc/>
        public void DeleteSave(XElement element)
        {
            XDocument xdoc = XDocument.Load(FILENAME);

            xdoc.Descendants("save")
                .Where(x => (int)x.Attribute("id") == (int)element.Attribute("id"))
                .Remove();

            xdoc.Save(FILENAME);
        }

        /// <inheritdoc/>
        public XDocument GetAllSaves()
        {
            if (!File.Exists(FILENAME))
            {
                return null;
            }

            return XDocument.Load(FILENAME);
        }

        /// <inheritdoc/>
        public XElement GetOneSave(int id)
        {
            return this.GetAllSaves().Descendants("save").Where(x => (int)x.Attribute("id") == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public void SaveGame(XDocument xdoc)
        {
            if (xdoc != null && !string.IsNullOrEmpty(FILENAME))
            {
                xdoc.Save(FILENAME);
            }
        }
    }
}
