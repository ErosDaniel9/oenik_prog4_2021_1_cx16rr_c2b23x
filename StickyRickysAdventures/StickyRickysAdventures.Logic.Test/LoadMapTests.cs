﻿// <copyright file="LoadMapTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Logic.Test
{
    using NUnit.Framework;
    using StickyRickysAdventures.GameLogic;

    /// <summary>
    /// Test class for loadmap.
    /// </summary>
    public class LoadMapTests
    {
        /// <summary>
        /// Test for map loading from txt.
        /// </summary>
        /// <param name="fname">File name.</param>
        [TestCase("StickyRickysAdventures.GameLogic.Maps.test.txt")]
        public void TestLoadMapFromTxt(string fname)
        {
            GameModel.Models.MainGameModel model = new GameModel.Models.MainGameModel(335, 240);

            LoadMapLogic.LoadMapFromTxt(model, fname);

            Assert.That(model.Enemies.Count, Is.EqualTo(1));
            Assert.That(model.Platforms.GetLength(0), Is.EqualTo(5));
            Assert.That(model.Platforms.GetLength(1), Is.EqualTo(5));
        }
    }
}
