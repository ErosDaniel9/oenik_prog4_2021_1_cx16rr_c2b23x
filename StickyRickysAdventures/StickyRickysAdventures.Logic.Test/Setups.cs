﻿// <copyright file="Setups.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Logic.Test
{
    using System.Collections.Generic;
    using StickyRickysAdventures.GameModel.Sources.Classes;

    /// <summary>
    /// Setup class for tests.
    /// </summary>
    public static class Setups
    {
        /// <summary>
        /// Get simple model.
        /// </summary>
        /// <returns>Returns model.</returns>
        public static GameModel.Models.MainGameModel GetModel()
        {
            GameModel.Models.MainGameModel gamemodel = new GameModel.Models.MainGameModel(450, 800);

            int tileWidth = gamemodel.WindowWidth / 5;
            int tileHeight = gamemodel.WindowHeight / 5;

            Player player = new Player(1 * tileWidth, 3 * tileWidth, tileWidth, tileHeight);

            Platform[,] platforms = new Platform[5, 5];
            platforms[0, 4] = new Platform(0 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[1, 4] = new Platform(1 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[2, 4] = new Platform(2 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[3, 4] = new Platform(3 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[4, 4] = new Platform(4 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[3, 0] = new InteractablePlatform(3 * tileWidth, 0 * tileHeight, tileWidth, tileHeight);
            platforms[3, 1] = new InteractablePlatform(3 * tileWidth, 1 * tileHeight, tileWidth, tileHeight);
            platforms[3, 2] = new InteractablePlatform(3 * tileWidth, 2 * tileHeight, tileWidth, tileHeight);
            platforms[3, 3] = new InteractablePlatform(3 * tileWidth, 3 * tileHeight, tileWidth, tileHeight);

            gamemodel.Bullets = new List<Bullet>();

            gamemodel.TickSpeed = 16;
            gamemodel.Player = player;
            gamemodel.Platforms = platforms;
            gamemodel.TileHeight = gamemodel.WindowHeight / 5;
            gamemodel.TileWidth = gamemodel.WindowWidth / 5;

            return gamemodel;
        }

        /// <summary>
        /// Get model without ladders.
        /// </summary>
        /// <returns>Returns model.</returns>
        public static GameModel.Models.MainGameModel GetModelWithoutLadders()
        {
            GameModel.Models.MainGameModel gamemodel = new GameModel.Models.MainGameModel(450, 800);

            int tileWidth = gamemodel.WindowWidth / 5;
            int tileHeight = gamemodel.WindowHeight / 5;

            Player player = new Player(1 * tileWidth, 3 * tileHeight, tileWidth, tileHeight);

            Platform[,] platforms = new Platform[5, 5];
            platforms[0, 4] = new Platform(0 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[1, 4] = new Platform(1 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[2, 4] = new Platform(2 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[3, 4] = new Platform(3 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[4, 4] = new Platform(4 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);

            gamemodel.TickSpeed = 16;
            gamemodel.Player = player;
            gamemodel.Platforms = platforms;
            gamemodel.TileHeight = gamemodel.WindowHeight / 5;
            gamemodel.TileWidth = gamemodel.WindowWidth / 5;

            return gamemodel;
        }

        /// <summary>
        /// Get model with melee enemy on same position.
        /// </summary>
        /// <returns>Returns model.</returns>
        public static GameModel.Models.MainGameModel GetModelWithoutLaddersWithMeleeEnemyOnSamePositionAsPlayer()
        {
            GameModel.Models.MainGameModel gamemodel = new GameModel.Models.MainGameModel(450, 800);

            int tileWidth = gamemodel.WindowWidth / 5;
            int tileHeight = gamemodel.WindowHeight / 5;

            Player player = new Player(1 * tileWidth, 3 * tileHeight, tileWidth, tileHeight);

            Platform[,] platforms = new Platform[5, 5];
            platforms[0, 4] = new Platform(0 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[1, 4] = new Platform(1 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[2, 4] = new Platform(2 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[3, 4] = new Platform(3 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[4, 4] = new Platform(4 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);

            gamemodel.Bullets = new List<Bullet>();

            CombatGameItem enemy = new MeleeEnemy(1 * tileWidth, 3 * tileHeight, tileWidth, tileHeight)
            {
                CurrentHealth = 100,
                MaxHealth = 100,
            };
            gamemodel.Enemies.Add(enemy);

            gamemodel.TickSpeed = 16;
            gamemodel.Player = player;
            gamemodel.Platforms = platforms;
            gamemodel.TileHeight = gamemodel.WindowHeight / 5;
            gamemodel.TileWidth = gamemodel.WindowWidth / 5;

            return gamemodel;
        }

        /// <summary>
        /// Get model without ladder and with ranged enemy on same height.
        /// </summary>
        /// <returns>Returns model.</returns>
        public static GameModel.Models.MainGameModel GetModelWithoutLaddersWithRangedEnemyOnSameHeight()
        {
            GameModel.Models.MainGameModel gamemodel = new GameModel.Models.MainGameModel(450, 800);

            int tileWidth = gamemodel.WindowWidth / 5;
            int tileHeight = gamemodel.WindowHeight / 5;

            Player player = new Player(1 * tileWidth, 3 * tileHeight, tileWidth, tileHeight);

            Platform[,] platforms = new Platform[5, 5];
            platforms[0, 4] = new Platform(0 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[1, 4] = new Platform(1 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[2, 4] = new Platform(2 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[3, 4] = new Platform(3 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);
            platforms[4, 4] = new Platform(4 * tileWidth, 4 * tileHeight, tileWidth, tileHeight);

            gamemodel.Bullets = new List<Bullet>();

            gamemodel.Enemies.Add(new RangedEnemy(4 * tileWidth, 3 * tileHeight, tileWidth, tileHeight));

            gamemodel.TickSpeed = 16;
            gamemodel.Player = player;
            gamemodel.Platforms = platforms;
            gamemodel.TileHeight = gamemodel.WindowHeight / 5;
            gamemodel.TileWidth = gamemodel.WindowWidth / 5;

            return gamemodel;
        }
    }
}
