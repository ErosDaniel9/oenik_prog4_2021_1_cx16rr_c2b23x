var class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_player_movement_tests =
[
    [ "TestCharacterJump", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_player_movement_tests.html#a1ea220f9d7679a68d9609be013e6c163", null ],
    [ "TestCharacterMovesHorizontally", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_player_movement_tests.html#ae288a9bc08b9fbb1c0e23f7a446c0e72", null ],
    [ "TestCharacterMovesVerticallyWithLadder", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_player_movement_tests.html#a997ad1ac9e45e5490390607440e7bce9", null ],
    [ "TestCharacterTriesToMovesVerticallyWithoutLadder", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_player_movement_tests.html#a430df45ddaebb67b95ab3f508ffc4f59", null ]
];