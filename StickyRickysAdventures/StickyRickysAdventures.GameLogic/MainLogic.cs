﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Input;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameModel.Sources;
    using StickyRickysAdventures.GameModel.Sources.Classes;
    using StickyRickysAdventures.GameModel.Sources.Interfaces;
    using StickyRickysAdventures.Repository;

    /// <summary>
    /// Logic of the main game.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private const int G = 1;
        private readonly int framePerSec;
        private IGameModel model;
        private List<Platform> platforms;
        private int tick;
        private int tickTimeInAir;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainLogic"/> class.
        /// </summary>
        /// <param name="model">IGameModel.</param>
        public MainLogic(IGameModel model)
        {
            this.tickTimeInAir = 0;
            this.tick = 0;
            this.model = model;
            this.framePerSec = 1000 / this.model.TickSpeed;
            this.FillPlatforms();
        }

        /// <inheritdoc/>
        public bool CanContinue => this.model.Enemies.Count == 0;

        /// <inheritdoc/>
        public void OneTick()
        {
            this.model.MapChanging = false;
            this.PlayerTick();

            if (this.model.Enemies.Count != 0)
            {
                foreach (CombatGameItem enemy in this.model.Enemies)
                {
                    this.EnemyTick(enemy);
                }
            }

            if (this.model.Bullets.Count != 0)
            {
                for (int i = 0; i < this.model.Bullets.Count; i++)
                {
                    int numberOfBullets = this.model.Bullets.Count;
                    this.BulletTick(this.model.Bullets[i]);
                    if (numberOfBullets != this.model.Bullets.Count)
                    {
                        i--;
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void KeyDown(Key key)
        {
            if (this.model.Player.CurrentHealth > 0)
            {
                Rect rect;
                if (key == Key.A)
                {
                    this.TurnSprintingOff();
                    this.model.Player.DX = -(float)((this.model.Player.MovementSpeed * this.model.TileWidth) / this.framePerSec);
                    this.model.Player.Direction = StickyRickysAdventures.GameModel.Sources.Enums.Direction.Left;
                }

                if (key == Key.D)
                {
                    this.TurnSprintingOff();
                    this.model.Player.DX = (float)((this.model.Player.MovementSpeed * this.model.TileWidth) / this.framePerSec);
                    this.model.Player.Direction = StickyRickysAdventures.GameModel.Sources.Enums.Direction.Right;
                }

                if (key == Key.W)
                {
                    rect = new Rect(this.model.Player.GetX() + (this.model.TileWidth / 2) - (this.model.TileWidth / 12), this.model.Player.GetY(), this.model.TileWidth / 5, this.model.TileHeight);
                    if (this.platforms.Where(x => x is InteractablePlatform).Any(x => x.Position.IntersectsWith(rect)))
                    {
                        this.model.Player.DY = -(float)((this.model.Player.MovementSpeed * this.model.TileWidth) / this.framePerSec);
                        this.model.Player.IsClimbing = true;
                        this.model.Player.IsJumping = false;
                        this.model.Player.IsFalling = false;
                    }
                }

                if (key == Key.S)
                {
                    rect = new Rect(this.model.Player.GetX() + (this.model.TileWidth / 2) - (this.model.TileWidth / 12), this.model.Player.GetY(), this.model.TileWidth / 5, this.model.TileHeight);
                    if (this.platforms.Where(x => x is InteractablePlatform).Any(x => x.Position.IntersectsWith(rect)))
                    {
                        this.model.Player.DY = (float)((this.model.Player.MovementSpeed * this.model.TileWidth) / this.framePerSec);
                        this.model.Player.IsClimbing = true;
                        this.model.Player.IsJumping = false;
                        this.model.Player.IsFalling = false;
                    }
                }

                if (key == Key.Space)
                {
                    if (!this.model.Player.IsJumping && !this.model.Player.IsFalling)
                    {
                        this.model.Player.DY = -(float)Math.Sqrt(this.model.TileHeight * (6 / 5) * 2);
                        this.model.Player.IsJumping = true;
                        this.model.Player.IsClimbing = false;
                    }
                }

                if (key == Key.G)
                {
                    if (this.model.Player.HealthPotionCount > 0)
                    {
                        this.model.Player.HealthPotionCount--;
                        this.model.Player.CurrentHealth += Math.Min(this.model.Player.MaxHealth - this.model.Player.CurrentHealth, 50);
                    }
                }

                if (key == Key.LeftShift)
                {
                    this.TurnSprintingOn();
                }
            }
        }

        /// <inheritdoc/>
        public void KeyUp(Key key)
        {
            if (key == Key.A)
            {
                this.model.Player.DX = 0;
            }

            if (key == Key.D)
            {
                this.model.Player.DX = 0;
            }

            if (key == Key.W)
            {
                if (!this.model.Player.IsFalling && !this.model.Player.IsJumping)
                {
                    this.model.Player.DY = 0;
                }
            }

            if (key == Key.S)
            {
                if (!this.model.Player.IsFalling && !this.model.Player.IsJumping)
                {
                    this.model.Player.DY = 0;
                }
            }

            if (key == Key.Q)
            {
                this.model.Player.CurrentAttackType = this.model.Player.CurrentAttackType == StickyRickysAdventures.GameModel.Sources.Enums.AttackType.Melee ? StickyRickysAdventures.GameModel.Sources.Enums.AttackType.Ranged : StickyRickysAdventures.GameModel.Sources.Enums.AttackType.Melee;
            }

            if (key == Key.LeftShift)
            {
                this.TurnSprintingOff();
            }
        }

        /// <inheritdoc/>
        public void PlayerAttack()
        {
            if (this.Attack(this.model.Player))
            {
                this.model.Enemies.Where(x => x.Position.IntersectsWith(this.model.Player.Position)).ToList().ForEach(x => x.CurrentHealth -= this.model.Player.MeleeDamage);
                this.model.Enemies.ToList().ForEach(x => this.CheckEnemyDie(x));
            }
        }

        private bool Attack(ICombatGameItem combatGameItem)
        {
            if (combatGameItem.CurrentHealth >= 0 && combatGameItem.CanAttack)
            {
                combatGameItem.CanAttack = false;
                combatGameItem.AttackTimer.Start();
                if (combatGameItem.CurrentAttackType == StickyRickysAdventures.GameModel.Sources.Enums.AttackType.Melee)
                {
                    combatGameItem.IsAttacking = true;
                    this.tick = 0;
                    return true;
                }
                else if (combatGameItem.CurrentAttackType == StickyRickysAdventures.GameModel.Sources.Enums.AttackType.Ranged && combatGameItem.BulletCount > 0)
                {
                    combatGameItem.IsAttacking = true;
                    this.tick = 0;
                    Bullet bullet = new Bullet((int)combatGameItem.GetX(), (int)combatGameItem.GetY() + (this.model.TileHeight / 2), this.model.TileWidth / 4, this.model.TileHeight / 8);
                    bullet.DX = combatGameItem.Direction == StickyRickysAdventures.GameModel.Sources.Enums.Direction.Left ? -(float)((bullet.MovementSpeed * this.model.TileWidth) / this.framePerSec) : (float)((bullet.MovementSpeed * this.model.TileWidth) / this.framePerSec);
                    bullet.Direction = combatGameItem.Direction;
                    bullet.DY = 0;
                    bullet.Owner = combatGameItem;
                    this.model.Bullets.Add(bullet);
                    combatGameItem.BulletCount--;
                }
            }

            return false;
        }

        private void PlayerTick()
        {
            this.tick++;

            if (this.tick == 6 * (1000 / 20 / this.model.TickSpeed))
            {
                this.model.Player.IsAttacking = false;
            }

            if (!this.model.Player.IsFalling && !this.model.Player.IsJumping)
            {
                Rect rect = new (this.model.Player.GetX() + (this.model.TileWidth * 0.3), this.model.Player.GetY() + (this.model.TileHeight * 0.2), this.model.TileWidth - (this.model.TileWidth * 0.5), this.model.TileHeight * 0.85);
                if (!this.platforms.Any(x => x.Position.IntersectsWith(rect)))
                {
                    this.model.Player.DY = 1;
                    this.model.Player.IsFalling = true;
                    this.model.Player.IsClimbing = false;
                }
            }

            if (this.model.Player.DX != 0 || this.model.Player.DY != 0 || this.model.Player.IsFalling)
            {
                if (this.model.Player.DX != 0)
                {
                    this.PlayerMoveLeftOrRight();
                }

                if (this.model.Player.DY != 0 || this.model.Player.IsFalling)
                {
                    this.PlayerMoveUpOrDown();
                }

                if (this.model.Player.Position.IntersectsWith(this.model.EndPoint))
                {
                    this.PlayerIsOnExit();
                }
            }
        }

        private void PlayerMoveLeftOrRight()
        {
            float newX = (float)(this.model.Player.GetX() + this.model.Player.DX);
            float newY = (float)this.model.Player.GetY();
            Rect newRect = new Rect(newX + (this.model.TileWidth / 10), newY + (this.model.TileHeight * 0.15), this.model.TileWidth - ((this.model.TileWidth / 10) * 2), this.model.Player.Position.Height - (this.model.TileHeight * 0.18));

            if (newX >= 0 && (this.model.Player.Position.Right + this.model.Player.DX) < this.model.WindowWidth &&
                !this.platforms.Where(x => x is not InteractablePlatform).ToList().Any(x => x.Position.IntersectsWith(newRect)))
            {
                if (!this.platforms.Where(x => x is InteractablePlatform).ToList().Any(x => x.Position.IntersectsWith(newRect)))
                {
                    this.model.Player.IsClimbing = false;
                }

                this.MoveObject(this.model.Player, newX, newY);
            }
        }

        private void PlayerMoveUpOrDown()
        {
            float newX = (float)this.model.Player.GetX();
            float newY = (float)(this.model.Player.GetY() + this.model.Player.DY);
            Rect newRect = new Rect(this.model.Player.GetX() + (this.model.TileWidth / 4), this.model.Player.GetY() + this.model.Player.DY + (this.model.TileHeight / 10), this.model.TileWidth - (this.model.TileWidth / 3), this.model.TileHeight - (this.model.TileHeight / 10));
            if (newY >= 0 && newY < this.model.WindowHeight &&
                !this.platforms.Where(x => x is not InteractablePlatform).ToList().Any(x => x.Position.IntersectsWith(newRect)))
            {
                this.MoveObject(this.model.Player, newX, newY);
                if (this.model.Player.IsFalling)
                {
                    this.model.Player.DY += G + (this.tickTimeInAir++ / 10);
                }
                else if (this.model.Player.IsJumping)
                {
                    this.model.Player.DY += G;

                    if (this.model.Player.DY >= 0)
                    {
                        this.model.Player.IsFalling = true;
                        this.model.Player.IsJumping = false;
                    }
                }
                else
                {
                    this.model.Player.IsClimbing = true;
                }
            }
            else if (this.model.Player.DY > 0)
            {
                Rect rect = new Rect(this.model.Player.Position.Left + (this.model.TileWidth / 5), this.model.Player.Position.Bottom, this.model.TileWidth - (this.model.TileWidth / 5 * 2), this.model.Player.DY);
                List<Platform> platformsInWayDown = this.platforms.Where(x => x is not InteractablePlatform && x.Position.IntersectsWith(rect)).ToList();
                if (platformsInWayDown.Count != 0)
                {
                    this.MoveObject(this.model.Player, newX, (float)platformsInWayDown.Min(x => x.Position.Y) - this.model.TileHeight);
                }

                this.model.Player.IsClimbing = false;
                this.model.Player.DY = 0;
                this.tickTimeInAir = 0;
                this.model.Player.IsFalling = false;
            }
            else if (this.model.Player.DY < 0)
            {
                this.model.Player.DY = -this.model.Player.DY;
                this.model.Player.IsJumping = false;
            }
        }

        private void PlayerIsOnExit()
        {
            if (this.CanContinue)
            {
                this.model.MapChanging = true;
                int currentMapNumber = Convert.ToInt32(this.model.CurrentMap.Split(".").First());
                this.model.CurrentMap = (currentMapNumber + 1) < 10 ? $"0{currentMapNumber + 1}.txt" : $"{currentMapNumber + 1}.txt";
                this.model.Player.Score += 200;
                string filename = $"StickyRickysAdventures.GameLogic.Maps.{this.model.CurrentMap}";
                if (Assembly.GetExecutingAssembly().GetManifestResourceStream(filename) != null)
                {
                    LoadMapLogic.LoadMapFromTxt(this.model, filename);
                    this.FillPlatforms();
                }
                else
                {
                    this.model.NextMapExists = false;
                }
            }
        }

        private void EnemyTick(CombatGameItem enemy)
        {
            if (this.PlayerInSight(enemy) && this.model.Player.CurrentHealth > 0)
            {
                if (this.PlayerInRange(enemy))
                {
                    enemy.Direction = this.model.Player.GetX() < enemy.GetX() ? StickyRickysAdventures.GameModel.Sources.Enums.Direction.Left : StickyRickysAdventures.GameModel.Sources.Enums.Direction.Right;
                    if (this.Attack(enemy))
                    {
                        this.model.Player.CurrentHealth -= enemy.MeleeDamage;
                        this.model.Player.Score -= enemy.MeleeDamage * 10;
                        if (this.model.Player.CurrentHealth <= 0)
                        {
                            this.PlayerDeath();
                        }
                    }
                }
                else
                {
                    enemy.DX = enemy.GetX() < this.model.Player.GetX() ? ((float)((enemy.MovementSpeed * this.model.TileWidth) / this.framePerSec)) * 2 : (-(float)((enemy.MovementSpeed * this.model.TileWidth) / this.framePerSec) * 2);
                    enemy.Direction = enemy.DX < 0 ? StickyRickysAdventures.GameModel.Sources.Enums.Direction.Left : StickyRickysAdventures.GameModel.Sources.Enums.Direction.Right;
                    this.MoveEnemy(enemy);
                }
            }
            else
            {
                if (enemy.DX < 0)
                {
                    enemy.Direction = StickyRickysAdventures.GameModel.Sources.Enums.Direction.Left;
                }
                else if (enemy.DX > 0)
                {
                    enemy.Direction = StickyRickysAdventures.GameModel.Sources.Enums.Direction.Right;
                }

                enemy.DX = enemy.Direction == StickyRickysAdventures.GameModel.Sources.Enums.Direction.Left ? -(float)((enemy.MovementSpeed * this.model.TileWidth) / this.framePerSec) : (float)((enemy.MovementSpeed * this.model.TileWidth) / this.framePerSec);
                this.MoveEnemy(enemy);
                enemy.IsAttacking = false;
            }
        }

        private void MoveEnemy(CombatGameItem enemy)
        {
            int newX = (int)enemy.GetX();
            int newY = (int)enemy.GetY();
            Rect newRect = new (newX, newY + 2, this.model.TileWidth, this.model.TileHeight - 3);
            int xTile = newX / this.model.TileWidth;
            int yTile = newY / this.model.TileHeight;

            if ((newX < 0 ||
                xTile >= this.model.Platforms.GetLength(0)) ||
                this.platforms.Where(x => x is not InteractablePlatform).ToList().Any(x => x.Position.IntersectsWith(newRect)))
            {
                enemy.DX = -enemy.DX;
                newX = (int)(enemy.GetX() + enemy.DX);
            }
            else
            {
                if (enemy.DX > 0)
                {
                    xTile = (int)enemy.Position.TopRight.X / this.model.TileWidth;
                    yTile = (int)enemy.Position.TopRight.Y / this.model.TileHeight;
                    if (xTile >= this.model.Platforms.GetLength(0) || this.model.Platforms[xTile, yTile + 1] == null)
                    {
                        enemy.DX = -enemy.DX;
                        newX = (int)(enemy.GetX() + enemy.DX);
                    }
                }
                else if (enemy.DX < 0)
                {
                    xTile = (int)enemy.Position.TopLeft.X / this.model.TileWidth;
                    yTile = (int)enemy.Position.TopLeft.Y / this.model.TileHeight;
                    if (this.model.Platforms[xTile, yTile + 1] == null)
                    {
                        enemy.DX = -enemy.DX;
                        newX = (int)(enemy.GetX() + enemy.DX);
                    }
                }
            }

            this.MoveObject(enemy, (float)(enemy.GetX() + enemy.DX), newY);
        }

        private bool PlayerInSight(CombatGameItem enemy)
        {
            Rect rect;
            if (enemy.GetX() < this.model.Player.GetX())
            {
                rect = new Rect(enemy.GetX(), enemy.GetY() + (this.model.TileHeight * 0.5), this.model.Player.GetX() - enemy.GetX(), this.model.TileHeight * 0.25);
                return this.IsEnemyIntesectsWithPlayer(rect);
            }
            else
            {
                rect = new Rect(this.model.Player.GetX(), enemy.GetY() + (this.model.TileHeight * 0.5), enemy.GetX() - this.model.Player.GetX(), this.model.TileHeight * 0.25);
                return this.IsEnemyIntesectsWithPlayer(rect);
            }
        }

        private bool PlayerInRange(CombatGameItem enemy)
        {
            if (enemy is MeleeEnemy && Math.Abs(enemy.GetX() - this.model.Player.GetX()) <= this.model.TileWidth / 2)
            {
                return true;
            }
            else if (enemy is RangedEnemy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsEnemyIntesectsWithPlayer(Rect rec)
        {
            if (rec.IntersectsWith(this.model.Player.Position) &&
                this.platforms.Where(x => x is not InteractablePlatform).All(x => !x.Position.IntersectsWith(rec)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void BulletTick(Bullet bullet)
        {
            int newX = (int)(bullet.GetX() + bullet.DX);
            int newY = (int)bullet.GetY();

            if (bullet.Owner == this.model.Player)
            {
                foreach (CombatGameItem enemy in this.model.Enemies)
                {
                    if (enemy.Position.IntersectsWith(bullet.Position))
                    {
                        this.model.Bullets.Remove(bullet);
                        enemy.CurrentHealth -= this.model.Player.RangedDamage;
                        this.CheckEnemyDie(enemy);
                        return;
                    }
                }
            }
            else if (bullet.Owner != this.model.Player)
            {
                if (bullet.Position.IntersectsWith(this.model.Player.Position))
                {
                    this.model.Player.CurrentHealth -= bullet.Owner.RangedDamage;
                    this.model.Player.Score -= bullet.Owner.RangedDamage * 10;
                    this.model.Bullets.Remove(bullet);
                    if (this.model.Player.CurrentHealth <= 0)
                    {
                        this.PlayerDeath();
                    }

                    return;
                }
            }

            foreach (Platform platform in this.platforms)
            {
                if (platform is not InteractablePlatform && platform.Position.IntersectsWith(bullet.Position))
                {
                    this.model.Bullets.Remove(bullet);
                    return;
                }
            }

            if (bullet.GetX() < 0 || bullet.GetX() > this.model.WindowWidth)
            {
                this.model.Bullets.Remove(bullet);
                return;
            }

            this.MoveObject(bullet, (float)bullet.GetX() + bullet.DX, (float)bullet.GetY() + bullet.DY);
        }

        private void CheckEnemyDie(CombatGameItem enemy)
        {
            if (enemy.CurrentHealth <= 0)
            {
                this.model.Enemies.Remove(enemy);
                this.model.Player.CoinCount += 50;
                this.model.Player.Score += 500;
            }
        }

        private void PlayerDeath()
        {
            this.model.Player.DX = 0;
            this.model.Player.IsFalling = true;
        }

        private void MoveObject(GameItem cgi, float newX, float newY)
        {
            cgi.SetX(newX);
            cgi.SetY(newY);
        }

        private void FillPlatforms()
        {
            this.platforms = new List<Platform>();

            if (this.model.Platforms != null)
            {
                for (int i = 0; i < this.model.Platforms.GetLength(0); i++)
                {
                    for (int j = 0; j < this.model.Platforms.GetLength(1); j++)
                    {
                        if (this.model.Platforms[i, j] != null)
                        {
                            this.platforms.Add(this.model.Platforms[i, j]);
                        }
                    }
                }
            }
        }

        private void TurnSprintingOff()
        {
            if (this.model.Player.Sprinting)
            {
                this.model.Player.DX = (float)(this.model.Player.DX / 1.5);
                this.model.Player.Sprinting = false;
            }
        }

        private void TurnSprintingOn()
        {
            if (!this.model.Player.Sprinting)
            {
                this.model.Player.DX = (float)(this.model.Player.DX * 1.5);
                this.model.Player.Sprinting = true;
            }
        }
    }
}
