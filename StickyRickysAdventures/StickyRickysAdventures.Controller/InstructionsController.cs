﻿// <copyright file="InstructionsController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Controller
{
    using System.Windows;
    using System.Windows.Controls;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer;
    using StickyRickysAdventures.GameRenderer.Classes;

    /// <summary>
    /// A controller object for displaying our instruction screen.
    /// </summary>
    public class InstructionsController : FrameworkElement
    {
        private MainRenderer renderer;
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstructionsController"/> class.
        /// </summary>
        /// <param name="model">The model containing the gameplay data.</param>
        public InstructionsController(IGameModel model)
        {
            this.model = model;
            this.Loaded += this.InitController;
        }

        private void InitController(object sender, RoutedEventArgs e)
        {
            MainWindow win = Application.Current.MainWindow as MainWindow;
            if (win != null)
            {
                this.renderer = new MainRenderer(null, null, null, new InstructionsBuilder(this.model), null);
                Grid instructions = this.renderer.GetScoreBoard();
                win.Display.Children.Add(instructions);
                win.RegisterName("Instructions", instructions);
            }
        }
    }
}
