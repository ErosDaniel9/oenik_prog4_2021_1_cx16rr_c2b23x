﻿// <copyright file="IGameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Interfaces
{
    using System.Windows;

    /// <summary>
    /// Declares the basic methods of the GameItem.
    /// </summary>
    public interface IGameItem
    {
        /// <summary>
        /// Gets or sets the position of the element.
        /// </summary>
        public Rect Position { get; set; }

        /// <summary>
        /// Gets the x coordinate of the position rectangle.
        /// </summary>
        /// <returns>The x coordinate of the position rectangle.</returns>
        public double GetX();

        /// <summary>
        /// Gets the y coordinate of the position rectangle.
        /// </summary>
        /// <returns>The y coordinate of the position rectangle.</returns>
        public double GetY();

        /// <summary>
        /// Sets the x coordinate of the position rectangle.
        /// </summary>
        /// <param name="x">The new x component of the position rectangle.</param>
        public void SetX(double x);

        /// <summary>
        /// Sets the y coordinate of the position rectangle.
        /// </summary>
        /// <param name="y">The new y component of the position rectangle.</param>
        public void SetY(double y);
    }
}
