﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Don't need jagged array.")]
[assembly: SuppressMessage("Design", "CA1024:Use properties where appropriate", Justification = "Don't need props everywhere.")]
[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "No idea")]
