var interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter =
[
    [ "AttackSpeed", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#abafd2fd07256e0daddc74b40ad1fe4d5", null ],
    [ "AttackTimer", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#a5fa9880aadd5235192194f2562b71d8f", null ],
    [ "BulletCount", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#a8de8cbbf1b27dc1e3308e40a74bff4c2", null ],
    [ "CanAttack", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#a243699138b56297c30a2adf972fafe99", null ],
    [ "CurrentAttackType", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#a3d828055f3af2e811099c1c4ed85d5d2", null ],
    [ "CurrentHealth", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#a1e9be218251839f5e80b9a1c28eef099", null ],
    [ "IsAttacking", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#a9944cbb12a2f1bac1c5ce09264515708", null ],
    [ "MaxHealth", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#a38f419fd946752dc0e0fbf4ed63f5753", null ],
    [ "MeleeDamage", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#a92f1b48830136eedc4eb205c5cb56564", null ],
    [ "RangedDamage", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html#a3f74403fda896ac307b6fd5834dca236", null ]
];