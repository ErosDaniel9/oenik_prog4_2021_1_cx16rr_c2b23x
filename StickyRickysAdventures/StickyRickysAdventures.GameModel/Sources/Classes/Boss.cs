﻿// <copyright file="Boss.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Classes
{
    /// <summary>
    /// A descendant of the CombatGameItem object representing the boss.
    /// </summary>
    public class Boss : CombatGameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Boss"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the rect containing the boss.</param>
        /// <param name="y">The y coordinate of the rect containing the boss.</param>
        /// <param name="width">The width of the rect containing the boss.</param>
        /// <param name="height">The height of the rect containing the boss.</param>
        public Boss(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
        }
    }
}
