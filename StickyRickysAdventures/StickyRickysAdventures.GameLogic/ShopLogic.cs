﻿// <copyright file="ShopLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameLogic
{
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameModel.Sources.Enums;
    using StickyRickysAdventures.GameModel.Sources.Structs;

    /// <summary>
    /// Logic for shopping.
    /// </summary>
    public static class ShopLogic
    {
        /// <summary>
        /// Buy upgrade.
        /// </summary>
        /// <param name="gameModel">Gamemodel for chage.</param>
        /// <param name="shopItemWithCost">Upgrade from the shop.</param>
        /// <returns>Returns if purchase wwas succesfull.</returns>
        public static bool BuyUpgrades(IGameModel gameModel, ShopItemWithCost shopItemWithCost)
        {
            if (gameModel?.Player.CoinCount < shopItemWithCost.Cost)
            {
                return false;
            }

            gameModel.Player.CoinCount -= shopItemWithCost.Cost;

            if (shopItemWithCost.ShopItem is PowerUp)
            {
                if ((PowerUp)shopItemWithCost.ShopItem == PowerUp.AttackSpeed)
                {
                    gameModel.Player.AttackSpeed -= 0.10;
                }
                else if ((PowerUp)shopItemWithCost.ShopItem == PowerUp.Health)
                {
                    gameModel.Player.MaxHealth += 20;
                    gameModel.Player.CurrentHealth += 20;
                }
                else if ((PowerUp)shopItemWithCost.ShopItem == PowerUp.MeleeAttackDamage)
                {
                    gameModel.Player.MeleeDamage += 4;
                }
                else if ((PowerUp)shopItemWithCost.ShopItem == PowerUp.RangedAttackDamage)
                {
                    gameModel.Player.RangedDamage += 2;
                }
                else if ((PowerUp)shopItemWithCost.ShopItem == PowerUp.MovementSpeed)
                {
                    gameModel.Player.MovementSpeed += 0.2;
                }
            }
            else if (shopItemWithCost.ShopItem is Consumable)
            {
                if ((Consumable)shopItemWithCost.ShopItem == Consumable.HealthPotion)
                {
                    gameModel.Player.HealthPotionCount += 2;
                }
            }
            else if (shopItemWithCost.ShopItem is Service)
            {
                if ((Service)shopItemWithCost.ShopItem == Service.RefillBullets)
                {
                    gameModel.Player.BulletCount += 3;
                }
            }

            return true;
        }
    }
}
