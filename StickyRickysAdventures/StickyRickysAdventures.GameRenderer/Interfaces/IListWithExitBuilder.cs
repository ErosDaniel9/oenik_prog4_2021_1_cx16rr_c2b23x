﻿// <copyright file="IListWithExitBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Interfaces
{
    using System.Windows.Controls;

    /// <summary>
    /// Methods for adding list elements and an exit point to a grid.
    /// </summary>
    public interface IListWithExitBuilder
    {
        /// <summary>
        /// Adds list elements to the grid.
        /// </summary>
        /// <returns>The builder.</returns>
        IListWithExitBuilder AddListElements();

        /// <summary>
        /// Adds an exit point to the grid.
        /// </summary>
        /// <returns>The builder.</returns>
        IListWithExitBuilder AddExit();

        /// <summary>
        /// Adds the title to the grid.
        /// </summary>
        /// <returns>The builder.</returns>
        IListWithExitBuilder AddTitle();

        /// <summary>
        /// Returns the platform we're adding the elements to.
        /// </summary>
        /// <returns>The platforms we're adding the elements to.</returns>
        Grid GetHighScoreScreen();
    }
}
