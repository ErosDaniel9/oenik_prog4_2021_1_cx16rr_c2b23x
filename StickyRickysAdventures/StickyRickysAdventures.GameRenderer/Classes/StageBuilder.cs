﻿// <copyright file="StageBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Classes
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameModel.Sources.Classes;
    using StickyRickysAdventures.GameRenderer.Interfaces;

    /// <summary>
    /// The class responsible for rendering the gameplay.
    /// </summary>
    public class StageBuilder : IStageBuilder
    {
        private IGameModel gameModel;
        private DrawingGroup stage;

        private Drawing oldBackground;
        private Drawing oldPlayer;
        private Drawing oldPlatforms;
        private Drawing oldLadders;
        private Drawing oldVendor;
        private Drawing oldExit;
        private Pen temppen;
        private bool isDead;

        private Dictionary<string, Brush> brushes;

        /// <summary>
        /// Initializes a new instance of the <see cref="StageBuilder"/> class.
        /// </summary>
        /// <param name="gameModel">The gameModel containing the gameplay data.</param>
        public StageBuilder(IGameModel gameModel)
        {
            this.gameModel = gameModel;
            this.stage = new DrawingGroup();
            this.brushes = new Dictionary<string, Brush>();
            this.temppen = new Pen(Brushes.Black, 1);
            this.InitBrushes();
        }

        /// <inheritdoc/>
        public IStageBuilder AddBackground()
        {
            if (this.oldBackground == null)
            {
                Brush backgroundBrush = this.brushes["StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Background.png"];
                Geometry g = new RectangleGeometry(new Rect(0, 0, this.gameModel.WindowWidth, this.gameModel.WindowHeight));
                this.oldBackground = new GeometryDrawing(backgroundBrush, null, g);
            }

            this.stage.Children.Add(this.oldBackground);
            return this;
        }

        /// <inheritdoc/>
        public IStageBuilder AddEnemies(int pictureIndex)
        {
            DrawingGroup enemies = new DrawingGroup();
            foreach (var enemy in this.gameModel.Enemies)
            {
                string embeddedPath = string.Empty;
                if (enemy is MeleeEnemy)
                {
                    embeddedPath = "StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Melee";
                }
                else if (enemy is RangedEnemy)
                {
                    embeddedPath = "StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Ranged";
                }
                else
                {
                    embeddedPath = "StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Boss";
                }

                Brush enemyBrush = this.GetEnemyBrush(enemy, embeddedPath, pictureIndex);
                Geometry g = new RectangleGeometry(enemy.Position);
                enemies.Children.Add(new GeometryDrawing(enemyBrush, null, g));
            }

            this.stage.Children.Add(enemies);

            return this;
        }

        /// <inheritdoc/>
        public IStageBuilder AddLadders()
        {
            if (this.oldLadders == null || this.gameModel.MapChanging)
            {
                Brush ladderBrush = this.brushes["StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Ladder.png"];
                GeometryGroup g = new GeometryGroup();
                for (int x = 0; x < this.gameModel.Platforms.GetLength(0); x++)
                {
                    for (int y = 0; y < this.gameModel.Platforms.GetLength(1); y++)
                    {
                        if (this.gameModel.Platforms[x, y] is InteractablePlatform)
                        {
                            Geometry ladder = new RectangleGeometry(this.gameModel.Platforms[x, y].Position);
                            g.Children.Add(ladder);
                        }
                    }
                }

                this.oldLadders = new GeometryDrawing(ladderBrush, null, g);
            }

            this.stage.Children.Add(this.oldLadders);
            return this;
        }

        /// <inheritdoc/>
        public IStageBuilder AddPlatforms()
        {
            if (this.oldPlatforms == null || this.gameModel.MapChanging)
            {
                Brush platformBrush = this.brushes["StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Platform.png"];
                GeometryGroup g = new GeometryGroup();
                for (int x = 0; x < this.gameModel.Platforms.GetLength(0); x++)
                {
                    for (int y = 0; y < this.gameModel.Platforms.GetLength(1); y++)
                    {
                        if ((this.gameModel.Platforms[x, y] is Platform) && !(this.gameModel.Platforms[x, y] is InteractablePlatform || this.gameModel.Platforms[x, y] is InvisiblePlatform))
                        {
                            Geometry platform = new RectangleGeometry(this.gameModel.Platforms[x, y].Position);
                            g.Children.Add(platform);
                        }
                    }

                    this.oldPlatforms = new GeometryDrawing(platformBrush, null, g);
                }
            }

            this.stage.Children.Add(this.oldPlatforms);
            return this;
        }

        /// <inheritdoc/>
        public IStageBuilder AddPlayer(int pictureIndex)
        {
            Brush playerBrush = this.GetPlayerBrush(this.gameModel.Player, "StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Player", pictureIndex);
            Geometry g = new RectangleGeometry(this.gameModel.Player.Position);
            this.oldPlayer = new GeometryDrawing(playerBrush, null, g);

            this.stage.Children.Add(this.oldPlayer);
            return this;
        }

        /// <inheritdoc/>
        public IStageBuilder AddVendor(int pictureIndex)
        {
            Brush vendorBrush = this.brushes[$"StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Vendor.{pictureIndex}.png"];
            Geometry g = new RectangleGeometry(this.gameModel.Vendor);
            this.oldVendor = new GeometryDrawing(vendorBrush, null, g);

            this.stage.Children.Add(this.oldVendor);
            return this;
        }

        /// <inheritdoc/>
        public IStageBuilder AddBullets()
        {
            DrawingGroup bullets = new DrawingGroup();
            foreach (Bullet bullet in this.gameModel.Bullets)
            {
                Brush bulletBrush = this.brushes[$"StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Bullet.Move{bullet.Direction}.png"];
                Geometry g = new RectangleGeometry(bullet.Position);
                bullets.Children.Add(new GeometryDrawing(bulletBrush, null, g));
            }

            this.stage.Children.Add(bullets);
            return this;
        }

        /// <inheritdoc/>
        public IStageBuilder AddExit()
        {
            if (this.oldExit == null || this.gameModel.MapChanging || this.gameModel.Enemies.Count == 0)
            {
                Brush exitBrush = this.gameModel.Enemies.Count == 0 ?
                    this.brushes["StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.ExitOpen.png"] :
                    this.brushes["StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.ExitClosed.png"];
                Geometry g = new RectangleGeometry(this.gameModel.EndPoint);
                this.oldExit = new GeometryDrawing(exitBrush, null, g);
            }

            this.stage.Children.Add(this.oldExit);
            return this;
        }

        /// <inheritdoc/>
        public IStageBuilder AddEnemyHealthBars()
        {
            DrawingGroup healthBars = new DrawingGroup();
            foreach (CombatGameItem enemy in this.gameModel.Enemies)
            {
                double healthPct = enemy.CurrentHealth * 1.0 / enemy.MaxHealth;

                Pen border = new Pen(Brushes.Black, 1);
                Geometry filled = new RectangleGeometry(new Rect(enemy.GetX(), enemy.GetY() + 2, this.gameModel.TileWidth * healthPct, 10));
                healthBars.Children.Add(new GeometryDrawing(Brushes.Red, border, filled));

                Geometry empty = new RectangleGeometry(new Rect(filled.Bounds.Right, enemy.GetY() + 2, this.gameModel.TileWidth * (1 - healthPct), 10));
                healthBars.Children.Add(new GeometryDrawing(Brushes.Black, border, empty));
            }

            this.stage.Children.Add(healthBars);
            return this;
        }

        /// <inheritdoc/>
        public IStageBuilder AddHUD()
        {
            Pen border = new Pen(Brushes.Black, 1);

            Geometry healthBarFilled = this.AddHealthBarToHUD(border);
            double heightDiff;
            Geometry bullet = this.AddBulletToHUD(healthBarFilled, out heightDiff);
            Geometry coin = this.AddCoinToHUD(healthBarFilled, bullet, heightDiff);
            Geometry score = this.AddScoreToHUD();
            Geometry potion = this.AddPotionToHUD(healthBarFilled, coin, heightDiff);
            return this;
        }

        /// <inheritdoc/>
        public DrawingGroup GetStage()
        {
            DrawingGroup temp = this.stage.Clone();
            this.stage.Children.Clear();
            return temp;
        }

        /// <inheritdoc/>
        public void ClearCache()
        {
            this.oldPlayer = null;
            this.oldPlatforms = null;
            this.oldLadders = null;
            this.oldVendor = null;
            this.stage.Children.Clear();
            this.brushes.Clear();
        }

        private Geometry AddHealthBarToHUD(Pen border)
        {
            double healthPct = this.gameModel.Player.CurrentHealth >= 0 ? this.gameModel.Player.CurrentHealth * 1.0 / this.gameModel.Player.MaxHealth : 0;

            Geometry healthBarFilled = new RectangleGeometry(new Rect(5, 5, this.gameModel.WindowWidth / 3 * healthPct, 20));
            this.stage.Children.Add(new GeometryDrawing(Brushes.Red, border, healthBarFilled));

            Geometry healthBarEmpty = new RectangleGeometry(new Rect(healthBarFilled.Bounds.Right, healthBarFilled.Bounds.Top, this.gameModel.WindowWidth / 3 * (1 - healthPct), healthBarFilled.Bounds.Height));
            this.stage.Children.Add(new GeometryDrawing(Brushes.Black, border, healthBarEmpty));
            return healthBarFilled;
        }

        private Geometry AddBulletToHUD(Geometry healthBarFilled, out double heightDiff)
        {
            Brush bulletBrush = this.brushes["StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Bullet.MoveRight.png"];
            Geometry bullet = new RectangleGeometry(new Rect(
                healthBarFilled.Bounds.Left,
                healthBarFilled.Bounds.Bottom + (healthBarFilled.Bounds.Height * 0.2),
                healthBarFilled.Bounds.Height * 1.2,
                healthBarFilled.Bounds.Height));
            this.stage.Children.Add(new GeometryDrawing(bulletBrush, null, bullet));

            FormattedText bulletcount = new FormattedText(
                $"x {this.gameModel.Player.BulletCount}",
                CultureInfo.InvariantCulture,
                FlowDirection.LeftToRight,
                new Typeface("Franklin Gothic"),
                healthBarFilled.Bounds.Height * 5 / 4,
                Brushes.LightGray,
                1);
            heightDiff = (bulletcount.Height - bullet.Bounds.Height) / 2;
            Geometry bulletCountPos = bulletcount.BuildGeometry(new Point(bullet.Bounds.Right * 1.2, bullet.Bounds.Top - heightDiff));
            this.stage.Children.Add(new GeometryDrawing(Brushes.LightGray, new Pen(Brushes.Black, 1), bulletCountPos));

            return bullet;
        }

        private Geometry AddCoinToHUD(Geometry healthBarFilled, Geometry bullet, double heightDiff)
        {
            Brush coinBrush = this.brushes["StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Coin.png"];
            Geometry coin = new RectangleGeometry(new Rect(
                bullet.Bounds.Left,
                bullet.Bounds.Bottom + (bullet.Bounds.Height * 0.2),
                bullet.Bounds.Width,
                bullet.Bounds.Height));
            this.stage.Children.Add(new GeometryDrawing(coinBrush, null, coin));

            FormattedText coinCount = new FormattedText(
                $"x {this.gameModel.Player.CoinCount}",
                CultureInfo.InvariantCulture,
                FlowDirection.LeftToRight,
                new Typeface("Franklin Gothic"),
                healthBarFilled.Bounds.Height * 5 / 4,
                Brushes.Gold,
                1);
            Geometry coinCountPos = coinCount.BuildGeometry(new Point(coin.Bounds.Right * 1.2, coin.Bounds.Top - heightDiff));
            this.stage.Children.Add(new GeometryDrawing(Brushes.Gold, new Pen(Brushes.Goldenrod, 1), coinCountPos));

            return coin;
        }

        private Geometry AddScoreToHUD()
        {
            FormattedText score = new FormattedText(
                            $"{this.gameModel.Player.Score}",
                            CultureInfo.InvariantCulture,
                            FlowDirection.LeftToRight,
                            new Typeface("Franklin Gothic"),
                            this.gameModel.WindowHeight / 20,
                            Brushes.White,
                            1);

            Geometry scoreRect = score.BuildGeometry(new Point(this.gameModel.WindowWidth - (score.Width + 5), 0));
            this.stage.Children.Add(new GeometryDrawing(Brushes.White, new Pen(Brushes.Black, 1), scoreRect));
            return scoreRect;
        }

        private Geometry AddPotionToHUD(Geometry healthBarFilled, Geometry coin, double heightDiff)
        {
            Brush potionBrush = this.brushes["StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Potion.png"];
            Geometry potion = new RectangleGeometry(new Rect(
                coin.Bounds.Left,
                coin.Bounds.Bottom + (coin.Bounds.Height * 0.2),
                coin.Bounds.Width,
                coin.Bounds.Height));
            this.stage.Children.Add(new GeometryDrawing(potionBrush, null, potion));

            FormattedText potionCount = new FormattedText(
                $"x {this.gameModel.Player.HealthPotionCount}",
                CultureInfo.InvariantCulture,
                FlowDirection.LeftToRight,
                new Typeface("Franklin Gothic"),
                healthBarFilled.Bounds.Height * 5 / 4,
                Brushes.White,
                1);
            Geometry potionCountPos = potionCount.BuildGeometry(new Point(potion.Bounds.Right * 1.2, potion.Bounds.Top - heightDiff));
            this.stage.Children.Add(new GeometryDrawing(Brushes.White, new Pen(Brushes.MediumPurple, 1), potionCountPos));

            return coin;
        }

        private Brush GetPlayerBrush(Player player, string embeddedPath, int pictureIndex)
        {
            if (this.isDead)
            {
                return this.brushes[$"{embeddedPath}.Death.6.png"];
            }
            else if (player.CurrentHealth <= 0)
            {
                this.isDead = pictureIndex == 6;
                return this.brushes[$"{embeddedPath}.Death.{pictureIndex}.png"];
            }
            else if (player.IsAttacking)
            {
                return this.brushes[$"{embeddedPath}.{player.CurrentAttackType}Attack{player.Direction}.{pictureIndex}.png"];
            }
            else if (player.IsClimbing)
            {
                if (player.DY != 0)
                {
                    return this.brushes[$"{embeddedPath}.Climb.{pictureIndex}.png"];
                }
                else
                {
                    return this.brushes[$"{embeddedPath}.ClimbIdle.{pictureIndex}.png"];
                }
            }
            else if (player.IsJumping)
            {
                return this.brushes[$"{embeddedPath}.Jump{player.Direction}.{pictureIndex}.png"];
            }
            else if (player.IsFalling)
            {
                return this.brushes[$"{embeddedPath}.Fall{player.Direction}.{pictureIndex}.png"];
            }
            else if (player.DX != 0)
            {
                return this.brushes[$"{embeddedPath}.Move{player.Direction}.{pictureIndex}.png"];
            }
            else
            {
                return this.brushes[$"{embeddedPath}.Idle{player.Direction}.{pictureIndex}.png"];
            }
        }

        private Brush GetEnemyBrush(CombatGameItem enemy, string embeddedPath, int pictureIndex)
        {
            if (enemy.IsAttacking)
            {
                return this.brushes[$"{embeddedPath}.{enemy.CurrentAttackType}Attack{enemy.Direction}.{pictureIndex}.png"];
            }
            else
            {
                return this.brushes[$"{embeddedPath}.Move{enemy.Direction}.{pictureIndex}.png"];
            }
        }

        private void InitBrushes()
        {
            string[] folders = new string[] { "Boss", "Melee", "Player", "Ranged", "Vendor", "Bullet" };
            string[] movements = new string[] { "MeleeAttack", "RangedAttack", "Move", "Jump", "Idle", "Fall" };
            string[] directions = new string[] { "Left", "Right" };

            foreach (string folder in folders)
            {
                foreach (string movement in movements)
                {
                    foreach (string direction in directions)
                    {
                        for (int i = 1; i < 7; i++)
                        {
                            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.{folder}.{movement}{direction}.{i}.png", false);
                        }
                    }
                }
            }

            string[] staticAnimations = new string[] { "Death", "Hurt", "Climb", "ClimbIdle" };
            foreach (string folder in folders)
            {
                foreach (string animation in staticAnimations)
                {
                    for (int i = 1; i < 7; i++)
                    {
                        this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.{folder}.{animation}.{i}.png", false);
                    }
                }
            }

            for (int i = 1; i < 7; i++)
            {
                this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Vendor.{i}.png", false);
            }

            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Bullet.MoveLeft.png", false);
            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.InteractingObjects.Bullet.MoveRight.png", false);
            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Background.png", false);
            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Ladder.png", true);
            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Platform.png", true);
            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.ExitOpen.png", false);
            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.ExitClosed.png", false);
            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Coin.png", false);
            this.LoadBrush($"StickyRickysAdventures.GameRenderer.Resources.StageResources.StaticObjects.Potion.png", false);
        }

        private void LoadBrush(string fname, bool isTiled)
        {
            if (!this.brushes.ContainsKey(fname))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                if (bmp.StreamSource != null)
                {
                    ImageBrush ib = new ImageBrush(bmp);

                    if (isTiled)
                    {
                        ib.TileMode = TileMode.Tile;
                        ib.Viewport = new Rect(0, 0, this.gameModel.TileWidth, this.gameModel.TileHeight);
                        ib.ViewportUnits = BrushMappingMode.Absolute;
                    }

                    this.brushes.Add(fname, ib);
                    bmp.EndInit();
                }
            }
        }
    }
}
