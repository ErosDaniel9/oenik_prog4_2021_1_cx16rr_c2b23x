﻿// <copyright file="Platform.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Classes
{
    /// <summary>
    /// A class representing all platforms in the model.
    /// </summary>
    public class Platform : StaticGameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Platform"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the position rectangle.</param>
        /// <param name="y">The y coordinate of the position rectangle.</param>
        /// <param name="width">The width of the position rectangle.</param>
        /// <param name="height">The height of the position rectangle.</param>
        public Platform(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
        }
    }
}
