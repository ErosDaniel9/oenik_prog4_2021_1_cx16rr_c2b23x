﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Models
{
    using System.Collections.Generic;
    using System.Windows;
    using StickyRickysAdventures.GameModel.Sources.Classes;
    using StickyRickysAdventures.GameModel.Sources.Structs;

    /// <summary>
    /// All the properties of our GameModel.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether the next map exists or not.
        /// </summary>
        public bool NextMapExists { get; set; }

        /// <summary>
        /// Gets or sets the tick speed of the game.
        /// </summary>
        public int TickSpeed { get; set; }

        /// <summary>
        /// Gets or sets the width of the window displaying the data of the model.
        /// </summary>
        public int WindowWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the window displaying the data of the model.
        /// </summary>
        public int WindowHeight { get; set; }

        /// <summary>
        /// Gets or sets the object representing our player.
        /// </summary>
        public Player Player { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the map changed.
        /// </summary>
        public bool MapChanging { get; set; }

        /// <summary>
        /// Gets or sets a collection containing the enemies on the level.
        /// </summary>
        public IList<CombatGameItem> Enemies { get; set; }

        /// <summary>
        /// Gets or sets a collection containing the platforms on the level.
        /// </summary>
        public Platform[,] Platforms { get; set; }

        /// <summary>
        /// Gets or sets the rectangle containing the vendor.
        /// </summary>
        public Rect Vendor { get; set; }

        /// <summary>
        /// Gets a collection containing the powerups sold in the shop and their costs.
        /// </summary>
        public IList<ShopItemWithCost> ItemsSoldInShop { get; }

        /// <summary>
        /// Gets or sets a collection containing the currently active bullets.
        /// </summary>
        public IList<Bullet> Bullets { get; set; }

        /// <summary>
        /// Gets or sets the height of the tiles in the model.
        /// </summary>
        public int TileHeight { get; set; }

        /// <summary>
        /// Gets or sets the width of the tiles in the model.
        /// </summary>
        public int TileWidth { get; set; }

        /// <summary>
        /// Gets or sets the rectangle containing the end point of the map.
        /// </summary>
        public Rect EndPoint { get; set; }

        /// <summary>
        /// Gets or sets the boss on the current map.
        /// </summary>
        public Boss Boss { get; set; }

        /// <summary>
        /// Gets or sets the string containing the name of the current map.
        /// </summary>
        public string CurrentMap { get; set; }
    }
}
