﻿// <copyright file="MainRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
using System;

[assembly:CLSCompliant(false)]

namespace StickyRickysAdventures.GameRenderer
{
    using System.Windows.Controls;
    using System.Windows.Media;
    using StickyRickysAdventures.GameRenderer.Interfaces;

    /// <summary>
    /// The class responsible for rendering every object in the game.
    /// </summary>
    public class MainRenderer
    {
        private IStageBuilder stageBuilder;
        private IMenuBuilder menuBuilder;
        private IShopBuilder shopBuilder;
        private IListWithExitBuilder listWithExitBuilder;
        private IInputGridBuilder deathScreenBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainRenderer"/> class.
        /// </summary>
        /// <param name="stageBuilder">The builder building our gameplay stage.</param>
        /// <param name="menuBuilder">The builder building our menu.</param>
        /// <param name="shopBuilder">The builder building our shop.</param>
        /// <param name="listWithExitBuilder">The builder building our high score/instruction screen.</param>
        /// <param name="deathScreenBuilder">The builder building our death screen.</param>
        public MainRenderer(
            IStageBuilder stageBuilder,
            IMenuBuilder menuBuilder,
            IShopBuilder shopBuilder,
            IListWithExitBuilder listWithExitBuilder,
            IInputGridBuilder deathScreenBuilder)
        {
            this.stageBuilder = stageBuilder;
            this.menuBuilder = menuBuilder;
            this.shopBuilder = shopBuilder;
            this.listWithExitBuilder = listWithExitBuilder;
            this.deathScreenBuilder = deathScreenBuilder;
        }

        /// <summary>
        /// Return a drawing describing the gameplay.
        /// </summary>
        /// <param name="pictureIndex">The index of the selected picture in our animation sequence.</param>
        /// <returns>A drawing describing the gameplay.</returns>
        public Drawing GetGameplay(int pictureIndex)
        {
            if (pictureIndex == 0)
            {
                pictureIndex++;
            }

            this.stageBuilder.AddBackground().AddPlatforms().AddLadders().AddBullets().AddEnemies(pictureIndex).AddVendor(pictureIndex).AddPlayer(pictureIndex).AddExit().AddEnemyHealthBars().AddHUD();
            return this.stageBuilder.GetStage();
        }

        /// <summary>
        /// Returns the grid representing the menu.
        /// </summary>
        /// <returns>The grid representing themenu.</returns>
        public Grid GetMenu()
        {
            if (this.menuBuilder == null)
            {
                return null;
            }

            this.menuBuilder.InitMenu().AddButtons().AddTitle();
            return this.menuBuilder.GetMenu();
        }

        /// <summary>
        /// Returns the grid representing the shop.
        /// </summary>
        /// <returns>The grid representing the shop.</returns>
        public Grid GetShop()
        {
            if (this.shopBuilder == null)
            {
                return null;
            }

            this.shopBuilder.AddItems().AddExit().AddTitle().AddCoinCounter();
            return this.shopBuilder.GetShop();
        }

        /// <summary>
        /// Returns the grid representing the high score screen.
        /// </summary>
        /// <returns>The grid representing the high score screen.</returns>
        public Grid GetScoreBoard()
        {
            if (this.listWithExitBuilder == null)
            {
                return null;
            }

            this.listWithExitBuilder.AddTitle().AddListElements().AddExit();
            return this.listWithExitBuilder.GetHighScoreScreen();
        }

        /// <summary>
        /// Returns the grid representing the death screen.
        /// </summary>
        /// <returns>The grid representing the death screen.</returns>
        public Grid GetDeathScreen()
        {
            if (this.deathScreenBuilder == null)
            {
                return null;
            }

            this.deathScreenBuilder.AddMessage().AddInput().AddConfirmationButton();
            return this.deathScreenBuilder.GetInputGrid();
        }
    }
}
