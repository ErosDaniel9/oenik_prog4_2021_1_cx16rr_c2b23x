﻿// <copyright file="NameInputBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Classes
{
    using System;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer.Interfaces;

    /// <summary>
    /// A concrete implementation of the <see cref="IInputGridBuilder"/> interface, requests a name from the user.
    /// </summary>
    public class NameInputBuilder : IInputGridBuilder
    {
        private Grid input;
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="NameInputBuilder"/> class.
        /// </summary>
        /// <param name="model">The model containing the window data.</param>
        public NameInputBuilder(IGameModel model)
        {
            this.model = model;
            this.InitInput();
        }

        /// <inheritdoc/>
        public IInputGridBuilder AddConfirmationButton()
        {
            Button confirm = new Button();

            confirm.BorderBrush = null;
            confirm.Content = "Confirm";
            confirm.Foreground = Brushes.OrangeRed;
            confirm.FontStyle = FontStyles.Italic;
            confirm.Background = Brushes.Transparent;
            confirm.MaxHeight = this.input.Height * 0.1;
            confirm.MaxWidth = this.input.Width * 0.6;

            this.input.Children.Add(confirm);
            Grid.SetColumn(confirm, 0);
            Grid.SetRow(confirm, 5);

            return this;
        }

        /// <inheritdoc/>
        public IInputGridBuilder AddInput()
        {
            TextBox userInput = new TextBox();
            userInput.MaxWidth = this.input.Width * 0.7;
            userInput.TextAlignment = TextAlignment.Center;
            userInput.VerticalContentAlignment = VerticalAlignment.Center;
            this.input.Children.Add(userInput);
            Grid.SetColumn(userInput, 0);
            Grid.SetRow(userInput, 3);

            return this;
        }

        /// <inheritdoc/>
        public IInputGridBuilder AddMessage()
        {
            Label message = new Label();
            message.Content = "You died! Please enter your name...";
            message.HorizontalAlignment = HorizontalAlignment.Center;
            message.VerticalAlignment = VerticalAlignment.Center;
            message.FontFamily = new FontFamily("Papyrus");
            message.FontSize = 20;
            message.Tag = "Message";

            this.input.Children.Add(message);
            Grid.SetColumn(message, 0);
            Grid.SetRow(message, 1);

            return this;
        }

        /// <inheritdoc/>
        public Grid GetInputGrid()
        {
            return this.input;
        }

        private void InitInput()
        {
            this.input = new Grid();

            for (int i = 0; i < 7; i++)
            {
                this.input.RowDefinitions.Add(new RowDefinition());
            }

            this.input.ColumnDefinitions.Add(new ColumnDefinition());

            this.input.Width = this.model.WindowWidth * 0.2;
            this.input.Height = this.model.WindowHeight * 0.3;
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("StickyRickysAdventures.GameRenderer.Resources.MenuResources.In_Game_Menu_Background.png");
            if (bmp.StreamSource != null)
            {
                ImageBrush ib = new ImageBrush(bmp);
                this.input.Background = ib;
                bmp.EndInit();
            }
        }
    }
}
