﻿// <copyright file="HighScoreController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Controller
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using StickyRickysAdventures.GameLogic;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameModel.Sources.Structs;
    using StickyRickysAdventures.GameRenderer;
    using StickyRickysAdventures.GameRenderer.Classes;
    using StickyRickysAdventures.Repository;

    /// <summary>
    /// Controller repsponsible for displaying the 10 highest scores in the game.
    /// </summary>
    public class HighScoreController : FrameworkElement
    {
        private const int SCORELIMIT = 10;
        private MainRenderer renderer;
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighScoreController"/> class.
        /// </summary>
        /// <param name="model">The gamemodel containing the window data.</param>
        public HighScoreController(IGameModel model)
        {
            this.model = model;
            this.Loaded += this.InitController;
        }

        private void InitController(object sender, RoutedEventArgs e)
        {
            MainWindow win = Application.Current.MainWindow as MainWindow;
            if (win != null)
            {
                List<Score> highScores = HighscoreLogic.GetHighScores(new HighScoreRepository()).ToList();
                this.renderer = new MainRenderer(null, null, null, new HighScoreBuilder(this.model, SCORELIMIT, highScores), null);
                Grid scoreBoard = this.renderer.GetScoreBoard();
                win.Display.Children.Add(scoreBoard);
                win.RegisterName("HighScores", scoreBoard);
            }
        }
    }
}
