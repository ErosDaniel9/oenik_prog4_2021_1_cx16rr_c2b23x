﻿// <copyright file="MainMenuBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Classes
{
    using System;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer.Interfaces;

    /// <summary>
    /// A concrete implementation of the IMenuBuilder interface.
    /// </summary>
    public class MainMenuBuilder : IMenuBuilder
    {
        private const int BUTTONOFFSET = 3;
        private string[] buttonNames;
        private Grid menu;
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuBuilder"/> class. If the caller doesnt call any of the init methods at the start of the call chain, the class will use a grid with 3*(buttonNames.Length + 2) cells.
        /// </summary>
        /// <param name="model">The gameModel containing the window data.</param>
        /// <param name="buttonNames">the name of the buttons we want to add to the grid.</param>
        public MainMenuBuilder(IGameModel model, string[] buttonNames)
        {
            this.model = model;
            this.buttonNames = buttonNames == null ? Array.Empty<string>() : buttonNames;
        }

        /// <inheritdoc/>
        public IMenuBuilder AddButtons()
        {
            for (int i = 0; i < this.buttonNames.Length; i++)
            {
                Button menuElement = new Button();

                menuElement.BorderBrush = null;
                menuElement.Content = this.buttonNames[i];
                menuElement.Foreground = Brushes.White;
                menuElement.FontStyle = FontStyles.Italic;
                menuElement.Background = Brushes.DarkGreen;
                menuElement.MaxHeight = this.menu.Height / (this.buttonNames.Length + (BUTTONOFFSET * 2));
                menuElement.MaxWidth = this.menu.Width * 0.7 / 3;
                menuElement.BorderBrush = Brushes.Brown;
                menuElement.BorderThickness = new Thickness(menuElement.MaxHeight * 0.05);

                this.menu.Children.Add(menuElement);
                Grid.SetColumn(menuElement, 1);
                Grid.SetRow(menuElement, i + 3);
            }

            return this;
        }

        /// <inheritdoc/>
        public IMenuBuilder InitMenu()
        {
            this.menu = new Grid();
            for (int i = 0; i < this.buttonNames.Length + 5; i++)
            {
                this.menu.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < 3; i++)
            {
                this.menu.ColumnDefinitions.Add(new ColumnDefinition());
            }

            this.menu.Width = this.model.WindowWidth;
            this.menu.Height = this.model.WindowHeight;

            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("StickyRickysAdventures.GameRenderer.Resources.MenuResources.Main_Menu_Background.png");
            if (bmp.StreamSource != null)
            {
                ImageBrush ib = new ImageBrush(bmp);
                this.menu.Background = ib;
                bmp.EndInit();
            }

            return this;
        }

        /// <inheritdoc/>
        public Grid GetMenu()
        {
            return this.menu;
        }

        /// <inheritdoc/>
        public IMenuBuilder AddTitle()
        {
            Label title = new Label();
            title.Content = "Sticky Rickys Adventures";
            title.Foreground = Brushes.AliceBlue;
            title.Background = Brushes.Transparent;
            title.FontFamily = new FontFamily("Papyrus");
            title.FontSize = 20;
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.VerticalAlignment = VerticalAlignment.Center;

            this.menu.Children.Add(title);
            Grid.SetColumn(title, 1);
            Grid.SetRow(title, 1);
            return this;
        }
    }
}
