﻿// <copyright file="MainMenuController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Controller
{
    using System.Windows;
    using System.Windows.Controls;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer;
    using StickyRickysAdventures.GameRenderer.Classes;

    /// <summary>
    /// The controller handling the main menu.
    /// </summary>
    public class MainMenuController : FrameworkElement
    {
        private IGameModel gameModel;
        private MainRenderer gameRenderer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenuController"/> class.
        /// </summary>
        public MainMenuController()
        {
            this.Loaded += this.InitControl;
        }

        private void InitControl(object sender, RoutedEventArgs e)
        {
            MainWindow win = Application.Current.MainWindow as MainWindow;
            if (win != null)
            {
                this.gameModel = new MainGameModel((int)this.ActualWidth, (int)this.ActualHeight);
                this.gameModel.TickSpeed = 16;

                string[] buttonNames = new string[] { "New Game", "Load Game", "High Scores", "Instructions", "Exit Game" };
                this.gameRenderer = new MainRenderer(null, new MainMenuBuilder(this.gameModel, buttonNames), null, null, null);

                Grid menu = this.gameRenderer.GetMenu();

                win.Display.Children.Add(menu);
                win.RegisterName("Menu", menu);
            }
        }
    }
}
