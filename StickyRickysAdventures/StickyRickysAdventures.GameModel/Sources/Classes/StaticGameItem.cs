﻿// <copyright file="StaticGameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources
{
    /// <summary>
    /// The abstract class representing the game items whose position is the same throughout the gameplay.
    /// </summary>
    public class StaticGameItem : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StaticGameItem"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the position rectangle.</param>
        /// <param name="y">The y coordinate of the position rectangle.</param>
        /// <param name="width">The width of the position rectangle.</param>
        /// <param name="height">The height of the position rectangle.</param>
        public StaticGameItem(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
        }
    }
}
