﻿// <copyright file="HighscoreLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using StickyRickysAdventures.GameModel.Sources.Structs;
    using StickyRickysAdventures.Repository;

    /// <summary>
    /// Logic class for highscores.
    /// </summary>
    public static class HighscoreLogic
    {
        /// <summary>
        /// Add new highscore.
        /// </summary>
        /// <param name="highScoreRepository">Highscore repo.</param>
        /// <param name="name">Name for the highscore.</param>
        /// <param name="score">Score.</param>
        public static void NewHighscore(IHighScoreRepository highScoreRepository, string name, int score)
        {
            XElement element = new XElement("score", new XAttribute("name", name), score);

            highScoreRepository?.UpdateScoreBoard(element);
        }

        /// <summary>
        /// Get every highscore.
        /// </summary>
        /// <param name="highScoreRepository">Highscore repo.</param>
        /// <returns>Retrurns a List of highscores.</returns>
        public static IList<Score> GetHighScores(IHighScoreRepository highScoreRepository)
        {
            List<Score> scores = new ();

            XDocument xdoc = highScoreRepository?.GetHighScores();

            foreach (var item in xdoc.Descendants("score"))
            {
                scores.Add(new Score(item.Attribute("name").Value, Convert.ToInt32(item.Value)));
            }

            return scores;
        }
    }
}
