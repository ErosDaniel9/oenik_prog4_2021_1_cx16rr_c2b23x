﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Performance", "CA1014:MarkAssembliesWithCLSCompliant", Justification = "No idea")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "Don't need to be static")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "Don't need specified formatprovider.")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "Don't need jagged array.")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "Don't need secure random.")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = ".")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "Garbage collector helps.")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1137:Elements should have the same indentation", Justification = "Nope, they should not.")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1118", Justification = "Nope, they should not.")]
