﻿// <copyright file="CombatGameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Classes
{
    using System.Windows.Threading;
    using StickyRickysAdventures.GameModel.Sources.Enums;
    using StickyRickysAdventures.GameModel.Sources.Interfaces;

    /// <summary>
    /// A GameItem class implementing the IMover and IFighter interfaces.
    /// </summary>
    public class CombatGameItem : GameItem, ICombatGameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CombatGameItem"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the position rectangle.</param>
        /// <param name="y">The y coordinate of the position rectangle.</param>
        /// <param name="width">The width of the position rectangle.</param>
        /// <param name="height">The height of the position rectangle.</param>
        public CombatGameItem(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
            this.AttackTimer = new DispatcherTimer();
            this.AttackTimer.Tick += this.ResetAttack;
            this.CanAttack = true;
        }

        /// <inheritdoc/>
        public int CurrentHealth { get; set; }

        /// <inheritdoc/>
        public int MeleeDamage { get; set; }

        /// <inheritdoc/>
        public int RangedDamage { get; set; }

        /// <inheritdoc/>
        public int BulletCount { get; set; }

        /// <inheritdoc/>
        public AttackType CurrentAttackType { get; set; }

        /// <inheritdoc/>
        public float DX { get; set; }

        /// <inheritdoc/>
        public float DY { get; set; }

        /// <inheritdoc/>
        public bool IsAttacking { get; set; }

        /// <inheritdoc/>
        public Direction Direction { get; set; }

        /// <inheritdoc/>
        public int MaxHealth { get; set; }

        /// <inheritdoc/>
        public double MovementSpeed { get; set; }

        /// <inheritdoc/>
        public double AttackSpeed { get; set; }

        /// <inheritdoc/>
        public bool CanAttack { get; set; }

        /// <inheritdoc/>
        public DispatcherTimer AttackTimer { get; set; }

        private void ResetAttack(object sender, System.EventArgs e)
        {
            this.CanAttack = true;
            this.AttackTimer.Stop();
        }
    }
}
