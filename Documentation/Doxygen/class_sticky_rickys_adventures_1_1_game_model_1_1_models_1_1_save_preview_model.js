var class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model =
[
    [ "SavePreviewModel", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model.html#a03dec3a300205977f1c9d0b44ae11d9f", null ],
    [ "ToString", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model.html#a23f32ebc970b5ffb9a59e74be037a2cd", null ],
    [ "CurrentMap", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model.html#a3da68c8d2021c20b32911aceea617969", null ],
    [ "PlayerHp", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model.html#afb6771a96cd24a6684988576607eaa5a", null ],
    [ "PlayerScore", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model.html#a7fa6e23139977430e130b494e767a186", null ],
    [ "SaveID", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model.html#a03081fe040afb4c11c7bc5fe9f657137", null ]
];