﻿// <copyright file="InGameMenuBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameRenderer.Classes
{
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameRenderer.Interfaces;

    /// <summary>
    /// The class building our in game menu.
    /// </summary>
    public class InGameMenuBuilder : IMenuBuilder
    {
        private Grid menu;
        private string[] buttonNames;
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="InGameMenuBuilder"/> class.
        /// </summary>
        /// <param name="model">The model representing the gameplay data.</param>
        /// <param name="buttonNames">The name/content of the menu buttons.</param>
        public InGameMenuBuilder(IGameModel model, string[] buttonNames)
        {
            this.buttonNames = buttonNames;
            this.model = model;
        }

        /// <inheritdoc/>
        public IMenuBuilder AddButtons()
        {
            for (int i = 0; i < this.buttonNames.Length; i++)
            {
                Button menuElement = new Button();

                menuElement.BorderBrush = null;
                menuElement.Content = this.buttonNames[i];
                menuElement.Foreground = Brushes.OrangeRed;
                menuElement.FontStyle = FontStyles.Italic;
                menuElement.Background = Brushes.Transparent;
                menuElement.MaxHeight = this.menu.Height * 0.1;
                menuElement.MaxWidth = this.menu.Width * 0.6;

                this.menu.Children.Add(menuElement);
                Grid.SetColumn(menuElement, 0);
                Grid.SetRow(menuElement, i + 2);
            }

            return this;
        }

        /// <inheritdoc/>
        public IMenuBuilder AddTitle()
        {
            Label title = new Label();
            title.Content = "Menu";
            title.Foreground = Brushes.White;
            title.Background = Brushes.Transparent;
            title.FontSize = 15;
            title.FontFamily = new FontFamily("Papyrus");
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.VerticalAlignment = VerticalAlignment.Center;

            this.menu.Children.Add(title);
            Grid.SetColumn(title, 0);
            Grid.SetRow(title, 1);
            return this;
        }

        /// <inheritdoc/>
        public Grid GetMenu()
        {
            return this.menu;
        }

        /// <inheritdoc/>
        public IMenuBuilder InitMenu()
        {
            this.menu = new Grid();
            for (int i = 0; i < this.buttonNames.Length + 3; i++)
            {
                this.menu.RowDefinitions.Add(new RowDefinition());
            }

            this.menu.ColumnDefinitions.Add(new ColumnDefinition());

            this.menu.Width = this.model.WindowWidth * 0.1;
            this.menu.Height = this.model.WindowHeight * 0.3;
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("StickyRickysAdventures.GameRenderer.Resources.MenuResources.In_Game_Menu_Background.png");
            if (bmp.StreamSource != null)
            {
                ImageBrush ib = new ImageBrush(bmp);
                this.menu.Background = ib;
                bmp.EndInit();
            }

            return this;
        }
    }
}
