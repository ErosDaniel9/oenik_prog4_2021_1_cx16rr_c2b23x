﻿// <copyright file="AttackTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Logic.Test
{
    using System.Linq;
    using System.Windows.Input;
    using NUnit.Framework;

    /// <summary>
    /// Test class for attacks.
    /// </summary>
    public class AttackTests
    {
        /// <summary>
        /// Test for players melee attack.
        /// </summary>
        [Test]
        public void TestPlayerAttacksWithMelee()
        {
            GameModel.Models.MainGameModel model = Setups.GetModelWithoutLaddersWithMeleeEnemyOnSamePositionAsPlayer() as GameModel.Models.MainGameModel;
            GameLogic.MainLogic logic = new GameLogic.MainLogic(model);
            int enemyHpBeforeAttack = model.Enemies.Select(x => x.CurrentHealth).FirstOrDefault();

            logic.PlayerAttack();
            logic.OneTick();
            int enemyHpAfterAttack = model.Enemies.Select(x => x.CurrentHealth).FirstOrDefault();

            Assert.That(enemyHpBeforeAttack, Is.Not.EqualTo(enemyHpAfterAttack));
        }

        /// <summary>
        /// Test for players ranged attack.
        /// </summary>
        [Test]
        public void TestPlayerAttacksWithRanged()
        {
            GameModel.Models.MainGameModel model = Setups.GetModel() as GameModel.Models.MainGameModel;
            GameLogic.MainLogic logic = new GameLogic.MainLogic(model);

            logic.KeyUp(Key.Q);
            logic.PlayerAttack();
            logic.OneTick();
            Assert.That(model.Bullets.Count, Is.Not.EqualTo(0));
        }

        /// <summary>
        /// Test for enemy melee attack.
        /// </summary>
        [Test]
        public void TestMeleeEnemyAttack()
        {
            GameModel.Models.MainGameModel model = Setups.GetModelWithoutLaddersWithMeleeEnemyOnSamePositionAsPlayer() as GameModel.Models.MainGameModel;
            GameLogic.MainLogic logic = new GameLogic.MainLogic(model);

            logic.OneTick();
            Assert.That(model.Player.CurrentHealth, Is.Not.EqualTo(model.Player.MaxHealth));
        }

        /// <summary>
        /// Test for enemy ranged attack.
        /// </summary>
        [Test]
        public void RangedEnemyAttack()
        {
            GameModel.Models.MainGameModel model = Setups.GetModelWithoutLaddersWithRangedEnemyOnSameHeight() as GameModel.Models.MainGameModel;
            GameLogic.MainLogic logic = new GameLogic.MainLogic(model);

            logic.OneTick();
            Assert.That(model.Bullets.Count, Is.Not.EqualTo(0));
        }
    }
}
