var interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model =
[
    [ "Boss", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#a80fccec628fa3071a8142f5c7e2c7034", null ],
    [ "Bullets", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#a1b115ed99e58e3244bc268d2839609e0", null ],
    [ "CurrentMap", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#a380d75103036516208a8fa45340a4779", null ],
    [ "Enemies", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#a1d3c4c8caf3188fe129772bd52169f33", null ],
    [ "Exit", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#a6fa82492005b10dabb4d4d289f5c2122", null ],
    [ "ItemsSoldInShop", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#a843ac8b57d013f0f83466c78ec67e3e8", null ],
    [ "MapChanging", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#a0b1656201ecb224c47dd93ef8786c94e", null ],
    [ "Platforms", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#aafed8b22196003ebe88223ef8d4be7d0", null ],
    [ "Player", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#a63217abfb1e002fcfb7b875d59e2e262", null ],
    [ "Rect", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#ab49b2feb21e265a471a0eaa454b4ebaf", null ],
    [ "TickSpeed", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#a2e5774214b1a44aa3e3c55ed446b9658", null ],
    [ "TileHeight", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#abec0e7934efa9ea2f9a96e09b0fe7846", null ],
    [ "TileWidth", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#ae5c9205935d5f55cca687ace259f330d", null ],
    [ "Vendor", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#aec109b50aac79ba983c3d080957fddd5", null ],
    [ "WindowHeight", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#ae8197cd98f74563fd3959f0f03e77f66", null ],
    [ "WindowWidth", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html#af1e500bd571f019d7cd18af623c83309", null ]
];