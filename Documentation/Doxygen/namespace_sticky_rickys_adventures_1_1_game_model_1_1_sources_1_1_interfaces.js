var namespace_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces =
[
    [ "ICombatGameItem", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_combat_game_item.html", null ],
    [ "IFighter", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter" ],
    [ "IGameItem", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item.html", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item" ],
    [ "IMover", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_mover.html", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_mover" ]
];