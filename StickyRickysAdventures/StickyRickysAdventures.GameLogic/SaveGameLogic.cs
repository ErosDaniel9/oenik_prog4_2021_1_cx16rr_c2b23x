﻿// <copyright file="SaveGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameLogic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameModel.Sources.Classes;
    using StickyRickysAdventures.Repository;

    /// <summary>
    /// Class for saveegame logic.
    /// </summary>
    public class SaveGameLogic : ISaveGameLogic
    {
        private IGameModel gameModel;
        private IGameStateRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveGameLogic"/> class.
        /// </summary>
        /// <param name="model">GameModel that needs to be saved.</param>
        public SaveGameLogic(IGameModel model)
        {
            this.gameModel = model;
            this.repository = new GameStateRepository();
        }

        /// <summary>
        /// Saves the game.
        /// </summary>
        public void SaveGame()
        {
            XDocument xdoc = this.repository.GetAllSaves();
            int id = 0;

            if (xdoc == null)
            {
                xdoc = new XDocument();
                XElement basicElement = new XElement("saves");
                xdoc.Add(basicElement);
            }
            else
            {
                id = xdoc.XPathSelectElements("//saves/save").Max(x => (int)x.Attribute("id")) + 1;
            }

            List<Platform> platforms = this.PlatformsToList();

            XElement element = new XElement("save");
            element.Add(new XAttribute("id", id));
            element.Add(
                new XElement("currentmap", this.gameModel.CurrentMap),
                        new XElement(
                            "window",
                            new XElement(
                                "tiles",
                                new XElement("verticaltilecounter", this.gameModel.Platforms.GetLength(0)),
                                new XElement("horizontaltilecounter", this.gameModel.Platforms.GetLength(1)),
                                new XElement(
                                    "platforms",
                                    from p in platforms
                                    where p is not InteractablePlatform && p is not InvisiblePlatform
                                    select
    new XElement(
        "platform",
        new XElement("positionX", p.GetX() / this.gameModel.TileWidth),
        new XElement("positionY", p.GetY() / this.gameModel.TileHeight))),
                                new XElement(
                                    "ladders",
                                from p in platforms
                                where p is InteractablePlatform && p is not InvisiblePlatform
                                select
    new XElement(
        "ladder",
        new XElement("positionX", p.GetX() / this.gameModel.TileWidth),
        new XElement("positionY", p.GetY() / this.gameModel.TileHeight))),
                                new XElement(
                                    "invisibleplatforms",
                                from p in platforms
                                where p is InvisiblePlatform
                                select
                                new XElement(
                                    "invisibleplatform",
                                new XElement("positionX", p.GetX() / this.gameModel.TileWidth),
                                new XElement("positionY", p.GetY() / this.gameModel.TileHeight))))),
                        new XElement(
                            "player",
                            new XElement("score", this.gameModel.Player.Score),
                            new XElement("money", this.gameModel.Player.CoinCount),
                            new XElement("positionX", this.gameModel.Player.GetX() / this.gameModel.TileWidth),
                            new XElement("positionY", this.gameModel.Player.GetY() / this.gameModel.TileHeight),
                            new XElement("positionY", this.gameModel.Player.Direction),
                            new XElement("maxhealth", this.gameModel.Player.MaxHealth),
                            new XElement("currenthealth", this.gameModel.Player.CurrentHealth),
                            new XElement("meleedamage", this.gameModel.Player.MeleeDamage),
                            new XElement("rangeddamage", this.gameModel.Player.RangedDamage),
                            new XElement("currentattacktype", this.gameModel.Player.CurrentAttackType),
                            new XElement("bulletcounter", this.gameModel.Player.BulletCount),
                            new XElement("movementspeed", this.gameModel.Player.MovementSpeed),
                            new XElement("attackspeed", this.gameModel.Player.AttackSpeed),
                            new XElement("money", this.gameModel.Player.CoinCount),
                            new XElement("direction", this.gameModel.Player.Direction),
                            new XElement(
                                "consumables",
                                new XElement("healthpotions", this.gameModel.Player.HealthPotionCount))),
                        new XElement(
                            "vendor",
                            new XElement("positionX", this.gameModel.Vendor.X / this.gameModel.TileWidth),
                            new XElement("positionY", this.gameModel.Vendor.Y / this.gameModel.TileHeight)),
                        new XElement(
                            "exit",
                            new XElement("positionX", this.gameModel.EndPoint.X / this.gameModel.TileWidth),
                            new XElement("positionY", this.gameModel.EndPoint.Y / this.gameModel.TileHeight)),
                        new XElement(
                            "enemies",
                            new XElement(
                                "melees",
                            from m in this.gameModel.Enemies
                            where m is MeleeEnemy
                            select
new XElement(
    "melee",
new XElement("positionX", m.GetX() / this.gameModel.TileWidth),
new XElement("positionY", m.GetY() / this.gameModel.TileHeight),
new XElement("maxhealth", m.MaxHealth),
new XElement("currenthealth", m.CurrentHealth),
new XElement("movementspeed", m.MovementSpeed),
new XElement("attackspeed", m.AttackSpeed),
new XElement("direction", m.Direction),
    new XElement("damage", m.MeleeDamage))),
                            new XElement(
                                "rangeds",
                            from r in this.gameModel.Enemies
                            where r is RangedEnemy
                            select
new XElement(
    "ranged",
new XElement("positionX", r.GetX() / this.gameModel.TileWidth),
new XElement("positionY", r.GetY() / this.gameModel.TileHeight),
new XElement("maxhealth", r.MaxHealth),
new XElement("currenthealth", r.CurrentHealth),
new XElement("movementspeed", r.MovementSpeed),
new XElement("attackspeed", r.AttackSpeed),
new XElement("direction", r.Direction),
new XElement("bulletcounter", r.BulletCount),
new XElement("damage", r.RangedDamage))),
                            this.gameModel.Boss == null ?
                            new XElement("boss", string.Empty) :
                            new XElement(
                                "boss",
                                new XElement("positionX", this.gameModel.Boss.GetX() / this.gameModel.TileWidth),
                                new XElement("positionY", this.gameModel.Boss.GetY() / this.gameModel.TileHeight),
                                new XElement("maxhealth", this.gameModel.Boss.MaxHealth),
                                new XElement("currenthealth", this.gameModel.Boss.CurrentHealth),
                                new XElement("movementspeed", this.gameModel.Boss.MovementSpeed),
                                new XElement("attackspeed", this.gameModel.Boss.AttackSpeed),
                                new XElement("currentattacktype", this.gameModel.Boss.CurrentAttackType),
                                new XElement("meleedamage", this.gameModel.Boss.MeleeDamage),
                                new XElement("rangeddamage", this.gameModel.Boss.RangedDamage))),
                        new XElement(
                            "bullets",
                        from b in this.gameModel.Bullets
                        select
new XElement(
    "bullet",
    new XElement("positionX", b.GetX() / this.gameModel.TileWidth),
    new XElement("positionY", b.GetY() / this.gameModel.TileHeight),
    new XElement("direction", b.Direction),
    new XElement("owner", b.OwnerString),
    new XElement("damage", b.Owner.RangedDamage),
    new XElement("movementspeed", b.MovementSpeed),
    new XElement("dx", b.DX),
    new XElement("width", b.Position.Width),
    new XElement("height", b.Position.Height))));

            xdoc.Element("saves").Add(element);
            this.repository.SaveGame(xdoc);
        }

        private List<Platform> PlatformsToList()
        {
            List<Platform> platforms = new ();

            for (int i = 0; i < this.gameModel.Platforms.GetLength(0); i++)
            {
                for (int j = 0; j < this.gameModel.Platforms.GetLength(1); j++)
                {
                    if (this.gameModel.Platforms[i, j] != null)
                    {
                        platforms.Add(this.gameModel.Platforms[i, j]);
                    }
                }
            }

            return platforms;
        }
    }
}
