﻿// <copyright file="LoadMapLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Windows;
    using System.Xml.Linq;
    using StickyRickysAdventures.GameModel.Models;
    using StickyRickysAdventures.GameModel.Sources.Classes;
    using StickyRickysAdventures.Repository;

    /// <summary>
    /// Class for every maploading function.
    /// </summary>
    public static class LoadMapLogic
    {
        private static Random r = new Random();

        /// <summary>
        /// Load map from saving.
        /// </summary>
        /// <param name="id">Map id.</param>
        /// <param name="repository">Gamestate repository.</param>
        /// <param name="model">Game model.</param>
        /// <param name="windowWidth">Window's actual width.</param>
        /// <param name="windowHeight">Window's actual height.</param>
        public static void LoadMapFromSave(int id, IGameStateRepository repository, IGameModel model, int windowWidth, int windowHeight)
        {
            int tilewidth;
            int tileheight;
            XElement element = repository?.GetOneSave(id);
            if (model != null)
            {
                model.Platforms = new Platform[Convert.ToInt32(element.Element("window").Element("tiles").Element("verticaltilecounter").Value), Convert.ToInt32(element.Element("window").Element("tiles").Element("horizontaltilecounter").Value)];
                tilewidth = windowWidth / model.Platforms.GetLength(0);
                tileheight = windowHeight / model.Platforms.GetLength(1);
                model.TileWidth = tilewidth;
                model.TileHeight = tileheight;
                model.CurrentMap = element.Element("currentmap").Value;
                foreach (var e in element.Element("window").Element("tiles").Element("platforms").Descendants("platform"))
                {
                    int tilePosX = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionX").Value, CultureInfo.InvariantCulture)));
                    int tilePosY = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionY").Value, CultureInfo.InvariantCulture)));

                    model.Platforms[tilePosX, tilePosY] = new Platform((int)(tilePosX * tilewidth), (int)(tilePosY * tileheight), tilewidth, tileheight);
                }

                foreach (var e in element.Element("window").Element("tiles").Element("ladders").Descendants("ladder"))
                {
                    int tilePosX = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionX").Value, CultureInfo.InvariantCulture)));
                    int tilePosY = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionY").Value, CultureInfo.InvariantCulture)));

                    model.Platforms[tilePosX, tilePosY] = new InteractablePlatform((int)(tilePosX * tilewidth), (int)(tilePosY * tileheight), tilewidth, tileheight);
                }

                foreach (var e in element.Element("window").Element("tiles").Element("invisibleplatforms").Descendants("invisibleplatform"))
                {
                    int tilePosX = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionX").Value, CultureInfo.InvariantCulture)));
                    int tilePosY = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionY").Value, CultureInfo.InvariantCulture)));

                    model.Platforms[tilePosX, tilePosY] = new InvisiblePlatform((int)(tilePosX * tilewidth), (int)(tilePosY * tileheight), tilewidth, tileheight);
                }

                int playerPosX = Convert.ToInt32(Math.Floor(Convert.ToDouble(element.Element("player").Element("positionX").Value, CultureInfo.InvariantCulture)));
                int playerPosY = Convert.ToInt32(Math.Floor(Convert.ToDouble(element.Element("player").Element("positionY").Value, CultureInfo.InvariantCulture)));
                model.Player = new Player(playerPosX * tilewidth, playerPosY * tileheight, tilewidth, tileheight);
                model.Player.MaxHealth = Convert.ToInt32(element.Element("player").Element("maxhealth").Value);
                model.Player.CurrentHealth = Convert.ToInt32(element.Element("player").Element("currenthealth").Value);
                model.Player.MeleeDamage = Convert.ToInt32(element.Element("player").Element("meleedamage").Value);
                model.Player.RangedDamage = Convert.ToInt32(element.Element("player").Element("rangeddamage").Value);
                string playerattacktype = element.Element("player").Element("currentattacktype").Value;
                switch (playerattacktype)
                {
                    case "Melee":
                        model.Player.CurrentAttackType = GameModel.Sources.Enums.AttackType.Melee;
                        break;
                    case "Ranged":
                        model.Player.CurrentAttackType = GameModel.Sources.Enums.AttackType.Ranged;
                        break;
                }

                model.Player.AttackTimer.Interval = TimeSpan.FromMilliseconds(1000 / model.Player.AttackSpeed);
                model.Player.BulletCount = Convert.ToInt32(element.Element("player").Element("bulletcounter").Value);
                model.Player.MovementSpeed = Convert.ToDouble(element.Element("player").Element("movementspeed").Value, CultureInfo.InvariantCulture);
                model.Player.AttackSpeed = Convert.ToDouble(element.Element("player").Element("attackspeed").Value, CultureInfo.InvariantCulture);
                model.Player.CoinCount = Convert.ToInt32(element.Element("player").Element("money").Value);
                model.Player.Score = Convert.ToInt32(element.Element("player").Element("score").Value);
                string direction = element.Element("player").Element("direction").Value;
                switch (direction)
                {
                    case "Left":
                        model.Player.Direction = GameModel.Sources.Enums.Direction.Left;
                        break;
                    case "Right":
                        model.Player.Direction = GameModel.Sources.Enums.Direction.Right;
                        break;
                    default:
                        break;
                }

                model.Player.HealthPotionCount = Convert.ToInt32(element.Element("player").Element("consumables").Element("healthpotions").Value);
                model.Player.CanAttack = false;
                model.Player.AttackTimer.Start();

                var vendorPosX = Math.Round(Convert.ToDouble(element.Element("vendor").Element("positionX").Value, CultureInfo.InvariantCulture), 2);
                var vendorPosY = Math.Round(Convert.ToDouble(element.Element("vendor").Element("positionY").Value, CultureInfo.InvariantCulture), 2);
                model.Vendor = new System.Windows.Rect(vendorPosX * tilewidth, vendorPosY * tileheight, tilewidth, tileheight);

                var exitPosX = Math.Round(Convert.ToDouble(element.Element("exit").Element("positionX").Value, CultureInfo.InvariantCulture), 2);
                var exitPosY = Math.Round(Convert.ToDouble(element.Element("exit").Element("positionY").Value, CultureInfo.InvariantCulture), 2);
                model.EndPoint = new System.Windows.Rect(exitPosX * tilewidth, exitPosY * tileheight, tilewidth / 2, tileheight / 2);

                foreach (var e in element.Element("enemies").Descendants("melee"))
                {
                    var meleeEnemyPosX = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionX").Value, CultureInfo.InvariantCulture)));
                    var meleeEnemyPosY = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionY").Value, CultureInfo.InvariantCulture)));
                    CombatGameItem enemy = new MeleeEnemy(meleeEnemyPosX * tilewidth, meleeEnemyPosY * tileheight, tilewidth, tileheight);
                    enemy.MaxHealth = Convert.ToInt32(e.Element("maxhealth").Value);
                    enemy.CurrentHealth = Convert.ToInt32(e.Element("currenthealth").Value);
                    enemy.MovementSpeed = Convert.ToInt32(e.Element("movementspeed").Value);
                    enemy.AttackSpeed = Convert.ToInt32(e.Element("attackspeed").Value);
                    string enemydirection = e.Element("direction").Value;
                    switch (enemydirection)
                    {
                        case "Left":
                            enemy.Direction = GameModel.Sources.Enums.Direction.Left;
                            break;
                        case "Right":
                            enemy.Direction = GameModel.Sources.Enums.Direction.Right;
                            break;
                    }

                    enemy.AttackTimer.Interval = TimeSpan.FromMilliseconds(1000 / enemy.AttackSpeed);
                    enemy.MeleeDamage = Convert.ToInt32(e.Element("damage").Value);
                    model.Enemies.Add(enemy);
                }

                foreach (var e in element.Element("enemies").Descendants("ranged"))
                {
                    var rangedEnemyPosX = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionX").Value, CultureInfo.InvariantCulture)));
                    var rangedEnemyPosY = Convert.ToInt32(Math.Floor(Convert.ToDouble(e.Element("positionY").Value, CultureInfo.InvariantCulture)));
                    CombatGameItem enemy = new RangedEnemy(rangedEnemyPosX * tilewidth, rangedEnemyPosY * tileheight, tilewidth, tileheight);
                    enemy.MaxHealth = Convert.ToInt32(e.Element("maxhealth").Value);
                    enemy.CurrentHealth = Convert.ToInt32(e.Element("currenthealth").Value);
                    enemy.MovementSpeed = Convert.ToInt32(e.Element("movementspeed").Value);
                    enemy.AttackSpeed = Convert.ToInt32(e.Element("attackspeed").Value);
                    string enemydirection = e.Element("direction").Value;
                    switch (enemydirection)
                    {
                        case "Left":
                            enemy.Direction = GameModel.Sources.Enums.Direction.Left;
                            break;
                        case "Right":
                            enemy.Direction = GameModel.Sources.Enums.Direction.Right;
                            break;
                    }

                    enemy.AttackTimer.Interval = TimeSpan.FromMilliseconds(1000 / enemy.AttackSpeed);
                    enemy.RangedDamage = Convert.ToInt32(e.Element("damage").Value);
                    model.Enemies.Add(enemy);
                }

                if (element.Element("enemies").Element("boss").Value.Length != 0)
                {
                    var bossPosX = Convert.ToInt32(Math.Floor(Convert.ToDouble(element.Element("enemies").Element("boss").Element("positionX").Value, CultureInfo.InvariantCulture)));
                    var bossPosY = Convert.ToInt32(Math.Floor(Convert.ToDouble(element.Element("enemies").Element("boss").Element("positionY").Value, CultureInfo.InvariantCulture)));
                    CombatGameItem boss = new Boss(bossPosX * tilewidth, bossPosY * tileheight, tilewidth, tileheight);
                    boss.MaxHealth = Convert.ToInt32(element.Element("enemies").Element("boss").Element("maxhealth").Value);
                    boss.CurrentHealth = Convert.ToInt32(element.Element("enemies").Element("boss").Element("currenthealth").Value);
                    boss.MovementSpeed = Convert.ToInt32(element.Element("enemies").Element("boss").Element("movementspeed").Value);
                    string bossattacktype = element.Element("enemies").Element("boss").Element("currentattacktype").Value;
                    switch (bossattacktype)
                    {
                        case "Melee":
                            model.Player.CurrentAttackType = GameModel.Sources.Enums.AttackType.Melee;
                            break;
                        case "Ranged":
                            model.Player.CurrentAttackType = GameModel.Sources.Enums.AttackType.Ranged;
                            break;
                    }

                    boss.MeleeDamage = Convert.ToInt32(element.Element("enemies").Element("boss").Element("meleedamage").Value);
                    boss.RangedDamage = Convert.ToInt32(element.Element("enemies").Element("boss").Element("rangeddamage").Value);
                    model.Boss = boss as Boss;
                }

                foreach (var e in element.Element("bullets").Descendants("bullet"))
                {
                    var bulletPosX = Convert.ToDouble(e.Element("positionX").Value, CultureInfo.InvariantCulture);
                    var bulletPosY = Convert.ToDouble(e.Element("positionY").Value, CultureInfo.InvariantCulture);
                    var width = Convert.ToDouble(e.Element("width").Value, CultureInfo.InvariantCulture);
                    var height = Convert.ToDouble(e.Element("height").Value, CultureInfo.InvariantCulture);
                    Bullet bullet = new Bullet((int)(bulletPosX * tilewidth), (int)(bulletPosY * tileheight), Convert.ToInt32(Math.Floor(width)), Convert.ToInt32(Math.Floor(height)));
                    string bulletdir = e.Element("direction").Value;
                    bullet.MovementSpeed = Convert.ToInt32(e.Element("movementspeed").Value);
                    bullet.DX = float.Parse(e.Element("dx").Value, CultureInfo.InvariantCulture);
                    switch (bulletdir)
                    {
                        case "Left":
                            bullet.Direction = GameModel.Sources.Enums.Direction.Left;
                            break;
                        case "Right":
                            bullet.Direction = GameModel.Sources.Enums.Direction.Right;
                            break;
                    }

                    string owner = e.Element("owner").Value;
                    switch (owner)
                    {
                        case "player":
                            bullet.Owner = model.Player;
                            break;
                        case "rangedEnemy":
                            RangedEnemy rangedEnemy = new RangedEnemy(-windowWidth, -windowHeight, 0, 0);
                            rangedEnemy.RangedDamage = Convert.ToInt32(e.Element("damage").Value);
                            bullet.Owner = rangedEnemy;
                            break;
                        case "boss":
                            Boss boss = new Boss(-windowWidth, -windowHeight, 0, 0);
                            boss.RangedDamage = Convert.ToInt32(e.Element("damage").Value);
                            bullet.Owner = boss;
                            break;
                    }

                    model.Bullets.Add(bullet);
                }
            }
        }

        /// <summary>
        /// Load map from txt.
        /// </summary>
        /// <param name="model">Game model.</param>
        /// <param name="fname">Filename of the map.</param>
        public static void LoadMapFromTxt(IGameModel model, string fname)
        {
            r = new Random();
            if (model != null)
            {
                model.Bullets = new List<Bullet>();
                model.CurrentMap = fname?.Substring(fname.Length - 6, 6);
                Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                StreamReader sr = new (stream);
                string[] lines = sr.ReadToEnd().Replace("\r", string.Empty).Split('\n');

                int width = int.Parse(lines[0]);
                int height = int.Parse(lines[1]);
                model.Platforms = new Platform[width, height];
                model.TileWidth = model.WindowWidth / width;
                model.TileHeight = model.WindowHeight / height;
                bool isVendorOnTheMap = false;

                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        char current = lines[y + 2][x];
                        if (current != 32)
                        {
                            if (current == 'T')
                            {
                                Platform platform = new Platform(x * model.TileWidth, y * model.TileHeight, model.TileWidth, model.TileHeight);
                                model.Platforms[x, y] = platform;
                            }

                            if (current == 'L')
                            {
                                InteractablePlatform ladder = new InteractablePlatform(x * model.TileWidth, y * model.TileHeight, model.TileWidth, model.TileHeight);
                                model.Platforms[x, y] = ladder;
                            }

                            if (current == 'I')
                            {
                                InvisiblePlatform iplatform = new InvisiblePlatform(x * model.TileWidth, y * model.TileHeight, model.TileWidth, model.TileHeight);
                                model.Platforms[x, y] = iplatform;
                            }

                            if (current == 'S')
                            {
                                if (!model.MapChanging)
                                {
                                    model.Player = new Player(x * model.TileWidth, y * model.TileHeight, model.TileWidth, model.TileHeight);
                                    model.Player.AttackTimer.Interval = TimeSpan.FromMilliseconds(1000 / model.Player.AttackSpeed);
                                }
                                else
                                {
                                    model.Player.SetX(x * model.TileWidth);
                                    model.Player.SetY(y * model.TileHeight);
                                }
                            }

                            if (current == 'M' || current == 'R' || current == 'B')
                            {
                                CombatGameItem enemy = null;
                                switch (current)
                                {
                                    case 'M':
                                        enemy = new MeleeEnemy(x * model.TileWidth, y * model.TileHeight, model.TileWidth, model.TileHeight);
                                        break;
                                    case 'R':
                                        enemy = new RangedEnemy(x * model.TileWidth, y * model.TileHeight, model.TileWidth, model.TileHeight);
                                        break;
                                    case 'B':
                                        enemy = new Boss(x * model.TileHeight, y * model.TileWidth, model.TileWidth, model.TileHeight);
                                        break;
                                }

                                enemy.AttackTimer.Interval = TimeSpan.FromMilliseconds(1000 / enemy.AttackSpeed);
                                int randomDir = r.Next(0, 2);
                                enemy.DX = randomDir == 0 ? -1 : 1;
                                enemy.Direction = randomDir == 0 ? GameModel.Sources.Enums.Direction.Left : GameModel.Sources.Enums.Direction.Right;
                                enemy.DY = 0;
                                model.Enemies.Add(enemy);
                            }

                            if (current == 'E')
                            {
                                model.EndPoint = new Rect((x * model.TileWidth) + (model.TileWidth / 2) - (model.TileWidth / 4), (y * model.TileHeight) + (model.TileHeight / 2) + 2, model.TileWidth / 2, model.TileHeight / 2);
                            }

                            if (current == 'V')
                            {
                                model.Vendor = new Rect(x * model.TileWidth, y * model.TileHeight, model.TileWidth, model.TileHeight);
                                isVendorOnTheMap = true;
                            }
                        }
                    }
                }

                if (!isVendorOnTheMap)
                {
                    model.Vendor = new Rect(-model.WindowWidth, -model.WindowHeight, 0, 0);
                }
            }
        }

        /// <summary>
        /// Gets every saved game.
        /// </summary>
        /// <param name="repository">Gamestate repository.</param>
        /// <returns>Returns a IList of SavePreviewModels.</returns>
        public static IList<ISavePreviewModel> GetEverySavedGame(IGameStateRepository repository)
        {
            List<ISavePreviewModel> savePreviewModels = new List<ISavePreviewModel>();
            XDocument xdoc = repository?.GetAllSaves();

            if (xdoc != null)
            {
                foreach (var element in xdoc.Descendants("save"))
                {
                    savePreviewModels.Add(new SavePreviewModel(Convert.ToInt32(element.Attribute("id").Value), Convert.ToInt32(element.Element("player").Element("score").Value), Convert.ToInt32(element.Element("player").Element("currenthealth").Value), element.Element("currentmap").Value));
                }
            }

            return savePreviewModels;
        }
    }
}
