﻿// <copyright file="IGameStateRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Repository
{
    using System.Xml.Linq;

    /// <summary>
    /// Declares the game's save and load functions.
    /// </summary>
    public interface IGameStateRepository
    {
        /// <summary>
        /// Returns every player made saves.
        /// </summary>
        /// <returns>XDocument that contains all of the savings.</returns>
        XDocument GetAllSaves();

        /// <summary>
        /// Returns the save with the given id.
        /// </summary>
        /// <param name="id">The id of the save.</param>
        /// <returns>An XElement containing all the data in the save.</returns>
        XElement GetOneSave(int id);

        /// <summary>
        /// Deletes a saved game from the xml.
        /// </summary>
        /// <param name="element">Expects an XElement, that contains information about the saving to be deleted.</param>
        void DeleteSave(XElement element);

        /// <summary>
        /// Saves the given XDocument into a file.
        /// </summary>
        /// <param name="xdoc">The XDocument to be saved.</param>
        void SaveGame(XDocument xdoc);
    }
}
