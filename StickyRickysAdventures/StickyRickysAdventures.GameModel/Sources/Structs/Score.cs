﻿// <copyright file="Score.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Structs
{
    using System;
    using System.Globalization;

    /// <summary>
    /// A struct containing all the data for needed saving and loading scores.
    /// </summary>
    public struct Score : IEquatable<Score>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Score"/> struct.
        /// </summary>
        /// <param name="name">The name of the player who earned the score.</param>
        /// <param name="value">The value of the score.</param>
        public Score(string name, int value)
        {
            this.Name = name;
            this.Value = value;
        }

        /// <summary>
        /// Gets or sets the name of the player who earned the score.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value of the score.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Returns a value indicating whether the two scores are equal.
        /// </summary>
        /// <param name="left">The left value.</param>
        /// <param name="right">The right value.</param>
        /// <returns>A value indicating whether the two scores are equal.</returns>
        public static bool operator ==(Score left, Score right) => left.Equals(right);

        /// <summary>
        /// Returns a value indicating whether the two scores are inequal.
        /// </summary>
        /// <param name="left">The left value.</param>
        /// <param name="right">The right value.</param>
        /// <returns>A value indicating whether the two scores are inequal.</returns>
        public static bool operator !=(Score left, Score right) => !left.Equals(right);

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return obj is Score score &&
                   this.Name == score.Name &&
                   this.Value == score.Value;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.Name.PadLeft(1)}.....{this.Value.ToString(CultureInfo.InvariantCulture).PadRight(5)}";
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <inheritdoc/>
        public bool Equals(Score other)
        {
            return this.Name == other.Name &&
                   this.Value == other.Value;
        }
    }
}
