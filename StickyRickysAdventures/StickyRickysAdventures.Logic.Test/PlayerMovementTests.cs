﻿// <copyright file="PlayerMovementTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Logic.Test
{
    using System;
    using System.Windows.Input;
    using NUnit.Framework;

    /// <summary>
    /// Test class for player movements.
    /// </summary>
    public class PlayerMovementTests
    {
        /// <summary>
        /// Test for player's horizontal moving.
        /// </summary>
        /// <param name="key">Key.</param>
        [TestCase(Key.A)]
        [TestCase(Key.D)]
        public void TestCharacterMovesHorizontally(Key key)
        {
            GameModel.Models.MainGameModel model = Setups.GetModel() as GameModel.Models.MainGameModel;
            GameLogic.MainLogic logic = new GameLogic.MainLogic(model);

            double playerX = model.Player.GetX();

            logic.KeyDown(key);
            logic.OneTick();
            double dx = model.Player.DX;
            Assert.That(playerX, Is.EqualTo(Math.Round(model.Player.GetX() - dx, 2)));
        }

        /// <summary>
        /// Test for player's vertically moving without ladder.
        /// </summary>
        /// <param name="key">Key.</param>
        [TestCase(Key.W)]
        [TestCase(Key.S)]
        public void TestCharacterTriesToMovesVerticallyWithoutLadder(Key key)
        {
            GameModel.Models.MainGameModel model = Setups.GetModelWithoutLadders() as GameModel.Models.MainGameModel;
            GameLogic.MainLogic logic = new GameLogic.MainLogic(model);

            double playerY = model.Player.GetY();

            logic.KeyDown(key);
            for (int i = 0; i < 100; i++)
            {
                logic.OneTick();
            }

            Assert.That(playerY, Is.EqualTo(Math.Round(model.Player.GetY(), 2)));
        }

        /// <summary>
        /// Test for player's vertically with ladder.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="playerPosition">Player's position.</param>
        [TestCase(Key.W, 300)]
        [TestCase(Key.S, 300)]
        public void TestCharacterMovesVerticallyWithLadder(Key key, int playerPosition)
        {
            GameModel.Models.MainGameModel model = Setups.GetModel() as GameModel.Models.MainGameModel;
            GameLogic.MainLogic logic = new GameLogic.MainLogic(model);
            model.Player.SetX(playerPosition);
            model.Player.MaxHealth = 100;
            model.Player.CurrentHealth = 100;

            double playerY = model.Player.GetY();

            logic.KeyDown(key);
            logic.OneTick();
            double dy = model.Player.DY;
            Assert.That(playerY, Is.EqualTo(Math.Round(model.Player.GetY() - dy, 2)));
        }

        /// <summary>
        /// Test for player's jumping.
        /// </summary>
        /// <param name="key">Key.</param>
        [TestCase(Key.Space)]
        public void TestCharacterJump(Key key)
        {
            GameModel.Models.MainGameModel model = Setups.GetModel() as GameModel.Models.MainGameModel;
            GameLogic.MainLogic logic = new GameLogic.MainLogic(model);

            double playerY = model.Player.GetY();

            logic.KeyDown(key);
            logic.OneTick();
            double dy = model.Player.DY;
            Assert.That(playerY, Is.EqualTo(Math.Round(model.Player.GetY() - (dy - 1), 2)));
        }
    }
}
