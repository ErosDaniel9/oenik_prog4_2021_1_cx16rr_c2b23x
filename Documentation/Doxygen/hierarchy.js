var hierarchy =
[
    [ "System.Windows.Application", null, [
      [ "StickyRickysAdventures.App", "class_sticky_rickys_adventures_1_1_app.html", null ],
      [ "StickyRickysAdventures.App", "class_sticky_rickys_adventures_1_1_app.html", null ],
      [ "StickyRickysAdventures.App", "class_sticky_rickys_adventures_1_1_app.html", null ],
      [ "StickyRickysAdventures.App", "class_sticky_rickys_adventures_1_1_app.html", null ]
    ] ],
    [ "StickyRickysAdventures.Logic.Test.AttackTests", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_attack_tests.html", null ],
    [ "StickyRickysAdventures.Logic.Test.EnemyMovementTests", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_enemy_movement_tests.html", null ],
    [ "FrameworkElement", null, [
      [ "StickyRickysAdventures.Controller.GameController", "class_sticky_rickys_adventures_1_1_controller_1_1_game_controller.html", null ],
      [ "StickyRickysAdventures.Controller.HighScoreController", "class_sticky_rickys_adventures_1_1_controller_1_1_high_score_controller.html", null ],
      [ "StickyRickysAdventures.Controller.InGameMenuController", "class_sticky_rickys_adventures_1_1_controller_1_1_in_game_menu_controller.html", null ],
      [ "StickyRickysAdventures.Controller.LoadGameController", "class_sticky_rickys_adventures_1_1_controller_1_1_load_game_controller.html", null ],
      [ "StickyRickysAdventures.Controller.MainMenuController", "class_sticky_rickys_adventures_1_1_controller_1_1_main_menu_controller.html", null ],
      [ "StickyRickysAdventures.Controller.MetaController", "class_sticky_rickys_adventures_1_1_controller_1_1_meta_controller.html", null ],
      [ "StickyRickysAdventures.Controller.ShopController", "class_sticky_rickys_adventures_1_1_controller_1_1_shop_controller.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameLogic.HighscoreLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_highscore_logic.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "StickyRickysAdventures.MainWindow", "class_sticky_rickys_adventures_1_1_main_window.html", null ],
      [ "StickyRickysAdventures.MainWindow", "class_sticky_rickys_adventures_1_1_main_window.html", null ],
      [ "StickyRickysAdventures.MainWindow", "class_sticky_rickys_adventures_1_1_main_window.html", null ]
    ] ],
    [ "IEquatable", null, [
      [ "StickyRickysAdventures.GameModel.Sources.Structs.Score", "struct_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_structs_1_1_score.html", null ],
      [ "StickyRickysAdventures.GameModel.Sources.Structs.ShopItemWithCost", "struct_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_structs_1_1_shop_item_with_cost.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameModel.Sources.Interfaces.IFighter", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_fighter.html", [
      [ "StickyRickysAdventures.GameModel.Sources.Interfaces.ICombatGameItem", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_combat_game_item.html", [
        [ "StickyRickysAdventures.GameModel.Sources.Classes.CombatGameItem", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_combat_game_item.html", [
          [ "StickyRickysAdventures.GameModel.Sources.Classes.Boss", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_boss.html", null ],
          [ "StickyRickysAdventures.GameModel.Sources.Classes.MeleeEnemy", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_melee_enemy.html", null ],
          [ "StickyRickysAdventures.GameModel.Sources.Classes.Player", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_player.html", null ],
          [ "StickyRickysAdventures.GameModel.Sources.Classes.RangedEnemy", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_ranged_enemy.html", null ]
        ] ]
      ] ]
    ] ],
    [ "IFormattable", null, [
      [ "StickyRickysAdventures.GameModel.Models.ISavePreviewModel", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_save_preview_model.html", [
        [ "StickyRickysAdventures.GameModel.Models.SavePreviewModel", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_save_preview_model.html", null ]
      ] ]
    ] ],
    [ "StickyRickysAdventures.GameModel.Sources.Interfaces.IGameItem", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_game_item.html", [
      [ "StickyRickysAdventures.GameModel.Sources.GameItem", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_game_item.html", [
        [ "StickyRickysAdventures.GameModel.Sources.Classes.Bullet", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet.html", null ],
        [ "StickyRickysAdventures.GameModel.Sources.Classes.CombatGameItem", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_combat_game_item.html", null ],
        [ "StickyRickysAdventures.GameModel.Sources.StaticGameItem", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_static_game_item.html", [
          [ "StickyRickysAdventures.GameModel.Sources.Classes.Platform", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_platform.html", [
            [ "StickyRickysAdventures.GameModel.Sources.Classes.InteractablePlatform", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_interactable_platform.html", null ]
          ] ]
        ] ]
      ] ],
      [ "StickyRickysAdventures.GameModel.Sources.Interfaces.ICombatGameItem", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_combat_game_item.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameModel.Models.IGameModel", "interface_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_i_game_model.html", [
      [ "StickyRickysAdventures.GameModel.Models.GameModel", "class_sticky_rickys_adventures_1_1_game_model_1_1_models_1_1_game_model.html", null ]
    ] ],
    [ "StickyRickysAdventures.Repository.IGameStateRepository", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_game_state_repository.html", [
      [ "StickyRickysAdventures.Repository.GameStateRepository", "class_sticky_rickys_adventures_1_1_repository_1_1_game_state_repository.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameRenderer.Interfaces.IHighScoreBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_high_score_builder.html", [
      [ "StickyRickysAdventures.GameRenderer.Classes.HighScoreBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_high_score_builder.html", null ]
    ] ],
    [ "StickyRickysAdventures.Repository.IHighScoreRepository", "interface_sticky_rickys_adventures_1_1_repository_1_1_i_high_score_repository.html", [
      [ "StickyRickysAdventures.Repository.HighScoreRepository", "class_sticky_rickys_adventures_1_1_repository_1_1_high_score_repository.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameRenderer.Interfaces.IInputGridBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_input_grid_builder.html", [
      [ "StickyRickysAdventures.GameRenderer.Classes.NameInputBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_name_input_builder.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameLogic.IMainLogic", "interface_sticky_rickys_adventures_1_1_game_logic_1_1_i_main_logic.html", [
      [ "StickyRickysAdventures.GameLogic.MainLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_main_logic.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameRenderer.Interfaces.IMenuBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_menu_builder.html", [
      [ "StickyRickysAdventures.GameRenderer.Classes.InGameMenuBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_in_game_menu_builder.html", null ],
      [ "StickyRickysAdventures.GameRenderer.Classes.LoadMenuBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_load_menu_builder.html", null ],
      [ "StickyRickysAdventures.GameRenderer.Classes.MainMenuBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_main_menu_builder.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameModel.Sources.Interfaces.IMover", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_mover.html", [
      [ "StickyRickysAdventures.GameModel.Sources.Classes.Bullet", "class_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_classes_1_1_bullet.html", null ],
      [ "StickyRickysAdventures.GameModel.Sources.Interfaces.ICombatGameItem", "interface_sticky_rickys_adventures_1_1_game_model_1_1_sources_1_1_interfaces_1_1_i_combat_game_item.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameLogic.ISaveGameLogic", "interface_sticky_rickys_adventures_1_1_game_logic_1_1_i_save_game_logic.html", [
      [ "StickyRickysAdventures.GameLogic.SaveGameLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_save_game_logic.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameRenderer.Interfaces.IShopBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_shop_builder.html", [
      [ "StickyRickysAdventures.GameRenderer.Classes.ShopBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_shop_builder.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameRenderer.Interfaces.IStageBuilder", "interface_sticky_rickys_adventures_1_1_game_renderer_1_1_interfaces_1_1_i_stage_builder.html", [
      [ "StickyRickysAdventures.GameRenderer.Classes.StageBuilder", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_classes_1_1_stage_builder.html", null ]
    ] ],
    [ "StickyRickysAdventures.GameLogic.LoadMapLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_load_map_logic.html", null ],
    [ "StickyRickysAdventures.Logic.Test.LoadMapTests", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_load_map_tests.html", null ],
    [ "StickyRickysAdventures.GameRenderer.MainRenderer", "class_sticky_rickys_adventures_1_1_game_renderer_1_1_main_renderer.html", null ],
    [ "StickyRickysAdventures.Logic.Test.PlayerMovementTests", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_player_movement_tests.html", null ],
    [ "StickyRickysAdventures.Logic.Test.Setups", "class_sticky_rickys_adventures_1_1_logic_1_1_test_1_1_setups.html", null ],
    [ "StickyRickysAdventures.GameLogic.ShopLogic", "class_sticky_rickys_adventures_1_1_game_logic_1_1_shop_logic.html", null ],
    [ "System.Windows.Window", null, [
      [ "StickyRickysAdventures.MainWindow", "class_sticky_rickys_adventures_1_1_main_window.html", null ],
      [ "StickyRickysAdventures.MainWindow", "class_sticky_rickys_adventures_1_1_main_window.html", null ],
      [ "StickyRickysAdventures.MainWindow", "class_sticky_rickys_adventures_1_1_main_window.html", null ],
      [ "StickyRickysAdventures.MainWindow", "class_sticky_rickys_adventures_1_1_main_window.html", null ]
    ] ]
];