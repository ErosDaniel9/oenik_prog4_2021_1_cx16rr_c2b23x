﻿// <copyright file="MeleeEnemy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.GameModel.Sources.Classes
{
    using StickyRickysAdventures.GameModel.Sources.Enums;

    /// <summary>
    /// Class representing the basic melee enemy.
    /// </summary>
    public sealed class MeleeEnemy : CombatGameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MeleeEnemy"/> class.
        /// </summary>
        /// <param name="x">The x coordinate of the position rectangle.</param>
        /// <param name="y">The y coordinate of the position rectangle.</param>
        /// <param name="width">The width of the position rectangle.</param>
        /// <param name="height">The height of the position rectangle.</param>
        public MeleeEnemy(int x, int y, int width, int height)
            : base(x, y, width, height)
        {
            this.MaxHealth = 80;
            this.CurrentHealth = 80;
            this.MeleeDamage = 10;
            this.RangedDamage = 0;
            this.CurrentAttackType = AttackType.Melee;
            this.BulletCount = 0;
            this.MovementSpeed = 1;
            this.AttackSpeed = 2;
        }
    }
}
