﻿// <copyright file="HighScoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace StickyRickysAdventures.Repository
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// A concrete implementation of the IHighScoreRepository interface.
    /// </summary>
    public class HighScoreRepository : IHighScoreRepository
    {
        private const string FILENAME = "highscores.xml";
        private const int SCORELIMIT = 10;

        /// <inheritdoc/>
        public XDocument GetHighScores()
        {
            if (File.Exists(FILENAME))
            {
                return XDocument.Load(FILENAME);
            }
            else
            {
                XDocument xdoc = new XDocument();
                xdoc.Add(new XElement("scores"));
                return xdoc;
            }
        }

        /// <inheritdoc/>
        public void UpdateScoreBoard(XElement score)
        {
            XDocument highScores = this.GetHighScores();
            if (highScores != null)
            {
                InsertScore(highScores, score);
                highScores = SortDocument(highScores);
                TrimDocument(highScores);
            }

            highScores.Save(FILENAME);
        }

        private static void TrimDocument(XDocument highScores)
        {
            int scores = highScores.Root.Elements().Count();
            if (scores > SCORELIMIT)
            {
                for (int i = SCORELIMIT; i < scores; i++)
                {
                    highScores.Descendants("score").ElementAt(i).Remove();
                }
            }
        }

        private static XDocument SortDocument(XDocument highScores)
        {
            return new XDocument(
                new XElement(
                    "scores",
                    highScores
                        .Descendants("score")
                        .OrderByDescending(x => int.Parse(x.Value, CultureInfo.InvariantCulture))));
        }

        private static void InsertScore(XDocument highScores, XElement score)
        {
            highScores.Element("scores").Add(score);
        }
    }
}
